<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ahp extends Admincore
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('ahp_model','model');
    }
    
    /* METHOD "READ"
     * berfungsi untuk membaca query dari database dengan system pagination
     */
    function index()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['unsur'] = core::getAll("unsur","gammu");
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
    
        
     /* METHOD "SEARCH"*/
    function search()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/search/include','',TRUE);
        $data['content'] =   $this->load->view('/search/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }   
    
    function create()
    {
        $email = $this->session->userdata('email');
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        if($email != "" && $data['level'] != "" & $data['username'] != "" ){
            if(isset($_REQUEST['nilai'])){
                $nilai =  $_REQUEST['nilai'];
                $id    =  $_REQUEST['id'];
                core::update("ahp","gammu", array(
                    "skala"=>$nilai,
                ),$id);
            }
            else{
            $data['ahp'] = core::manualQuery("SELECT ahp.id, ahp.skala, table1.nama_unsur kriteria1, table2.nama_unsur kriteria2 FROM ahp INNER JOIN (SELECT * FROM unsur) AS table1 ON table1.id_unsur = kriteria1 INNER JOIN (SELECT * FROM unsur) AS table2 ON table2.id_unsur = kriteria2 order by ahp.id ","gammu");
            
            $data['include'] =   $this->load->view('/create/include','',TRUE);
            $data['content'] =   $this->load->view('/create/content',$data,TRUE);
            $this->load->view("admin/main",$data);
            }
        }
    }

    /* METHOD "UPDATE"
     * berfungsi untuk mengupdate data dari database
     */
    function update($id = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters(' <ul class="help-block"><li class="text-error">', '</li></ul> ');  
        
          if(isset($_REQUEST['jsonObj']))
       {
            $data = $this->input->post('jsonObj');
            $tanggal = str_replace("/", "-", $data['birthday']) ;
            print_r($data);
            core::update('responden','gammu',array(
                'nama_responden'  => $data['nama'],
                'tempat_lahir' => $data['tempat_lahir'],
                'tanggal_lahir' => date("Y-m-d", strtotime($tanggal)),
                'usia' => $data['usia'],
                'alamat' => $data['alamat'],
                'jenis_kelamin' => $data['jenis_kelamin'],
                'pendidikan_terakhir' => $data['pendidikan'],
                'pekerjaan' => $data['pekerjaan'],
                'id_subunit' => $data['sub_unit'],
                'email' => $data['email'],
                'password' => sha1(md5($data['password'])),
                'telepon' => $data['telepon'],
            ), $id);

            // // variable untuk menampung data Pengguna
                $id_r = $id+1;
                core::update('admin','gammu',array(
                    'username' => $data['nama'],
                    'email' => $data['email'],
                    'password' => sha1(md5($data['password'])),
                    'created' => date("Y-m-d h:i:s"),
                    'level_id' => 3,
                ), $id_r); 
       }
       else{
            $data['level'] = $this->session->userdata('level_id');
        	$data['email'] = $this->session->userdata('email');
            $data['sub_unit'] = core::getAll('sub_unit','gammu'); 
            $data['include'] =   $this->load->view('/update/include','',TRUE);
            $data['content'] =   $this->load->view('/update/content',$data,TRUE);
            $this->load->view("admin/main",$data);
       }
    }
    
    function detail($id = '')
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/detail/include','',TRUE);
        $data['content'] =   $this->load->view('/detail/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
    // ACTIONS METHOD
    // method-method yang berfungsi hanya untuk sebuah actions/tidak ada view
    
    /* METHOD "DELETE"
     * berfungsi untuk menghapus data dari database
     */
    function delete($id = '')
    {
        $data = core::getWhere('ahp','gammu',array(
            'id'=>$id,
        ));
    }
    
    function pairwise()
    {
        $email = $this->session->userdata('email');
        $data['level']    = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['query'] = core::manualQuery("SELECT COUNT(id_unsur) AS jml from unsur","gammu");
        $data['kriteria'] = $this->getUnsur();
        $data['pairwise'] = core::getAll("pairwise", "gammu");
        foreach ($data['query']->result_array() as $row) {
            $data['jml'] = $row['jml'];
        }
        foreach ($data['kriteria']->result_array() as $key) {
          $x[] = $key['id_unsur'];
        }
        $unshift = array_unshift($x,"");
        $data['implode'] = implode(",", $x);
        
        $data['include']  =   $this->load->view('/read/include','',TRUE);
        $data['content']  =   $this->load->view('/read/pairwise',$data,TRUE);
        $this->load->view("admin/main",$data);   
    }

    function getUnsur()
    {
        $data = core::manualQuery("SELECT id_unsur,nama_unsur from unsur", "gammu");
        return $data;
    }

    function buatForm()
    {
        // $this->load->view("read/buat_form");   
    }

    function buatTabel()
    {
        // $this->load->view("read/buat_tabel");
    }

    function hitungAhp()
    {
        $this->load->view("read/hitung_ahp");
    }
}
?>
