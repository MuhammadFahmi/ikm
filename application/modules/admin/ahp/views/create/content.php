<!DOCTYPE html>
<html>
<head>
  <title></title>
  <?php //echo $this->load->view('config/css')?>
</head>
<body>
    <!-- loaders -->
      <div class="loaders"></div>
    <!-- /loaders -->                    
    <div class="x_title">
      <h2>Analytical Hierarchy Process <small>Users</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a href="#"><i class="fa fa-close"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    
    <div class="x_content">
      <!-- <a href="<?php echo base_url('ahp/create');?>"><button type="submit" name="tambah" class="btn btn-primary"><i class="fa fa-plus-square"></i> Tambah</button></a> --> <!-- <p class="text-muted font-13 m-b-30">
        Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
      </p> -->
       <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
          <tr class="headings">
            <th>No</th>
            <th>Kriteria 1</th>
            <th>Perbandingan</th>
            <th>Kriteria 2</th>
          </tr>
        </thead>
       
        <tbody style="color: ">
          <?php $no=1; foreach ($ahp->result() as $key ) {
            ?>
            <tr>
              <td><?php echo $no++?></td>
              <td><?php echo $key->kriteria1?></td>
              <td>
          <div class="item form-group">
            <div class="col-md-9 col-sm-9 col-xs-12">
              <select class="select2_multiple form-control" tabindex="-1" style="width: 240px;" id="single2" pk="<?php echo $key->id?>">
                <option value="empty">Pilih Perbandingan</option>
                <option value="1">Kriteria1 Sama Penting Dengan Kriteria2 (Nilai=1)</option>
                    
                <option value="3">Kriteria1 Sedikit Lebih Penting Dengan Kriteria2 (Nilai=3)</option>
                
                <option value="5">Kriteria1 Lebih Penting Dari Kriteria2 (Nilai=5)</option>
                
                <option value="7">Kriteria1 Jelas Sangat Penting Daripada Kriteria2 (Nilai=7)</option>
                
                <option value="9">Kriteria1 Mutlak Sangat Penting Daripada Kriteria2 (Nilai=9)</option>
                
                <option value="2">Kriteria1 Nilai Berdekatan Dengan Kriteria2 (Nilai=2)</option>
                
                <option value="0.33">Kriteria2 Sedikit Lebih Penting Dari Kriteria1 (Nilai=1/3)</option>
                
                <option value="0.2">Kriteria2 Lebih Penting Dari Kriteria1 (Nilai=1/5)</option>
                
                <option value="0.14">Kriteria2 Jeblas Sangat Penting Daripada Kriteria1 (Nilai=1/7)</option>
                
                <option value="0.11">Kriteria2 Mutlak Sangat Penting Daripada Kriteria1 (Nilai=1/9)</option>
                
                <option value="0.5">Kriteria2 Nilai Berdekatan Dengan Kriteria1 (Nilai=1/2)</option>
              </select>
            </div>
          </div></td>
              <td><?php echo $key->kriteria2?></td>
              <!-- 
              <td>

                <a href="<?php echo base_url();?>responden/delete/<?php echo $key->id_unsur?>"><button class="btn btn-danger" onclick="javascript: return confirm('yakin hapus ??')"><i class="fa fa-remove"></i> Hapus</button></a>

                <a href="<?php echo base_url();?>responden/update/<?php echo $key->id_unsur?>"><button class="btn btn-dark"><i class="fa fa-edit"></i> Update</button></a>


                </td> -->
            </tr>
          <?php } ?>
        </tbody>
      </table>

<?php echo $this->load->view('js'); ?>
</html>
</body>
