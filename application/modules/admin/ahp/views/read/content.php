<?php error_reporting(0);?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <?php echo $this->load->view('config/css')?>
</head>
<body>
    <!-- loaders -->
      <div class="loaders"></div>
    <!-- /loaders -->                    
    <div class="x_title">
      <h2>Analytical Hierarchy Process <small>Users</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a href="#"><i class="fa fa-close"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    
    <div class="x_content">
      <a href="<?php echo base_url('ahp/create');?>"><button type="submit" name="tambah" class="btn btn-primary"><i class="fa fa-plus-square"></i> Tambah</button></a> <!-- <p class="text-muted font-13 m-b-30">
        Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
      </p> -->
       <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
          <tr class="headings">
            <th>No</th>
            <th>Nama Unsur</th>
            <th>Eigenvector1</th>
            <th>Eigenvector2</th>
            <th>Jumlah</th>
            <th>Prioritas</th>
            <!-- <th></th> -->
          </tr>
        </thead>
       
        <tbody style="color: ">
          <?php $no=1; foreach ($unsur->result() as $key ) {
            ?>
            <tr>
              <td><?php echo $no++?></td>
              <td><?php echo $key->nama_unsur?></td>
              <td><?php //echo $key->tanggal_lahir?></td>
              <td><?php //echo $key->usia?></td>
              <td><?php //echo $key->pekerjaan?></td>
              <td><?php //echo $key->jenis_kelamin?></td>
              <!-- 
              <td>

                <a href="<?php echo base_url();?>responden/delete/<?php echo $key->id_unsur?>"><button class="btn btn-danger" onclick="javascript: return confirm('yakin hapus ??')"><i class="fa fa-remove"></i> Hapus</button></a>

                <a href="<?php echo base_url();?>responden/update/<?php echo $key->id_unsur?>"><button class="btn btn-dark"><i class="fa fa-edit"></i> Update</button></a>


                </td> -->
            </tr>
          <?php } ?>
        </tbody>
      </table>
<?php echo $this->load->view('config/include'); ?>
</html>
</body>