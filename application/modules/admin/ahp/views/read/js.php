<script type="text/javascript">
      $(document).ready(function(){
      //menentukan jumlah kriteria
     $("#ahp").submit(function() {       
       var jml = $("#jml_kriteria").val();
       $('#loading1').show();
        $.ajax({
           type: "POST",
           url : "<?php echo base_url('ahp/buatForm')?>",
           data: "jml="+jml+"",
           success: function(data){ 
          $('#loading1').hide();
          $('#hasil').show();
           document.getElementById("hasil").innerHTML = data;
       
           }   
         });
         return false;
      });
          
          //mengambil dari form kriteria
      $("#hasil").submit(function() {       
       var jml = $("#jumlah_kriteria").val();
       var i=1;
       var kriteria = new Array();
       for (i ; i <= jml; i++) {
        kriteria[i]=$('#kriteria-'+i+'').val();
       }
        $('#loading2').show();
        $.ajax({
           type: "POST",
           url : "<?php echo base_url('ahp/buatTabel')?>",    
         data: "jml="+jml+"&kriteria="+kriteria,
             success: function(data){ 
             $('#loading2').hide();
             $('#tabel').show();
             document.getElementById("tabel").innerHTML = data;
         }   
         });
         return false;
      });

          //mengambil dari form input pembobotan
       $("#input_ahp").submit(function() {
          //var n = $('#tabel2')[0].scrollHeight;
          $('#loading3').show();
        $.ajax({
           type: "POST",
           url : "<?php echo base_url('ahp/hitungAhp')?>",
           data: $("#input_ahp").serialize(),
             success: function(data){ 
            
             $('#loading3').hide();
             $('#tabel2').show();
           document.getElementById("tabel2").innerHTML = data;
           $('#mulai').show();
           $('#mulai').focus();
         }   
         });
         return false;
      });

    $("#mulai").click(function() {
          $('#hasil').hide();
           $('#tabel').hide();
           $('#tabel2').hide();
           $('#mulai').hide();
          return false;
     });


   $("#jml_kriteria").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
              
        event.preventDefault(); 
        
            }   
        }
    });
  });
  </script>
