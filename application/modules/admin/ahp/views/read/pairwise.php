<?php error_reporting(0);?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <?php //echo $this->load->view('config/css')?>
</head>
<body>
    <!-- loaders -->
      <div class="loaders"></div>
    <!-- /loaders -->                    
    
    <div class="x_title">
      <h2>Form Penghitungan Analytical Hierarcy Process (AHP)</h2>  
      <div class="clearfix"></div>
    </div>    

    <div class="">
      <div class="col-md-6 col-sm-6 col-xs-12" style="width: 100%">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-bars"></i> Keterangan <small>Float left</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Unsur</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Tabel Perbandingan</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Tabel Nilai Indeks Acak (RI)</a>
                </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                <table id="datatablse-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead style="background-color: #1abb9c;color: white">
                    <tr class="headings">
                      <th>No</th>
                      <th>ID Unsur</th>
                      <th>Nama Unsur</th>
                    </tr>
                  </thead>
                 
                  <tbody style="color: ">
                      <?php $no=1;foreach ($kriteria->result() as $key) {?>
                      <tr>
                        <td><?php echo $no++?></td>
                        <td><?php echo $key->id_unsur?></td>
                        <td><?php echo $key->nama_unsur?></td>
                      </tr>
                      <?php } ?>
                  </tbody>
                </table>

                </div>
                
                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                  <table id="datatablse-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead style="background-color: #1abb9c;color: white">
                    <tr class="headings">
                      <th>Skala</th>
                      <th>Definisi</th>
                      <th>Keterangan</th>
                    </tr>
                  </thead>
                 
                  <tbody style="color: ">
                      <?php foreach ($pairwise->result() as $key) {?>
                      <tr>
                        <td><?php echo $key->skala?></td>
                        <td><?php echo $key->definisi?></td>
                        <td><?php echo $key->keterangan?></td>
                      </tr>
                      <?php } ?>
                  </tbody>
                </table>

                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                  <table id="datatablse-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead style="background-color: #1abb9c;color: white">
                    <tr class="headings">
                      <th>n</th>
                      <?php 
                      for($i=1;$i<=15;$i++ ){
                      echo "<th>$i</th>";
                      }
                      ?>
                    </tr>
                  </thead>
                 
                  <tbody style="color: ">
                      <tr>
                        <td>RI</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0.58</td>
                        <td>0.90</td>
                        <td>1.12</td>
                        <td>1.24</td>
                        <td>1.32</td>
                        <td>1.41</td>
                        <td>1.45</td>
                        <td>1.49</td>
                        <td>1.51</td>
                        <td>1.48</td>
                        <td>1.56</td>
                        <td>1.57</td>
                        <td>1.59</td>
                      </tr>
                  </tbody>
                </table>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
  <div class='tabel'>
    <div class="x_content">
      <center><label style="font-size: 15px">Tanggal Survey<span class="required">*</span></label></center> 
      
      <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cari"><span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input id="tanggal_survey" class="form-control col-md-7 col-xs-12" required="required" type="text" placeholder="Cari ...." value="<?php echo $_GET['tanggal_survey']?>">
        </div>
          <button class="btn btn-primary" id="cari"><i class="fa fa-search"></i> Cari</button>
      </form>
      <form method="get">
      
      </div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#tanggal_survey').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                yearRange:"-100:+0",
                calender_style: "picker_1",
              }, 
               function(start, end, label) {
                 var years = moment().diff(start, 'years');
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>
      <div class="detail_survey"></div>          

      <script type="text/javascript">
        $(document).ready(function(){
          $("#cari").click(function(){
            var tanggal_survey = $("#tanggal_survey").val();
            if(tanggal_survey == ""){
              alert("harus diisi");
              // tanggal_survey.required;
            }
            else{ 
            $.ajax({
              type : "get",
              url  : "<?php echo base_url()?>kuesioner/searchNrr",
              data : "tanggal="+tanggal_survey,

              success:function(msg){
                $('.detail_survey').html(msg);
                // alert(msg);
              },
              error:function(msg){
                alert("error");
              
              },
            });
            }
          });
        });

      </script>
</form>
</div>

<center>  
  <form id="hasil" action="" method="post"></form>
  <b><h3>Tabel Pairwise Comparation</h3></b> 
  <form id="input_ahp" action="" method="post" style="overflow-x: auto;overflow-y: hidden " >
     <table id="tabel" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
  <?php
  $jml=$jml;
  $kolom=$jml+1;
  $data = explode(",", $implode);
  for ($i=0; $i<$kolom; $i++) { 
     echo "<tr>";
     for ($j=0; $j<$kolom; $j++) { 
        if($i==0){
           if(($i==0) && ($j==0)){
             echo "<td style='width:150px'><center>UNSUR</center></td>";
           } 
           else {
              echo "<td style='width:150px;background-color:#00FFFF;text-align:center;font-weight: bold;'>".$data[$j]."</td>";
           }
        }
        else {
          if($j==0){
            echo "<td style='width:150px;background-color:#00FFFF;text-align:center;font-weight: bold;'>".$data[$i]."</td>";
          }
          else if($i==$j){
            echo "<td style='width:150px;background-color:#D3D3D3'>1</td>";
          }
          else if($j>$i){
            echo "<td style='width:150px;background-color:white;'><input required = 'required' type='text' id='t-$i-$j' name='t-$i-$j' /></td>";
          }
          else {
            echo "<td style='width:150px;background-color:#40E0D0;font-weight: bold;'>0</td>";
          }
        }
     }
     echo "</tr><input type='hidden' id='jml_data' name='jml_data' value='$jml' />
           <input type='hidden' id='kriteria-$i' name='kriteria-$i' value='".$data[$i]."' />";
    }
     echo "<tr><td colspan='$i'><input type='submit' value='hitung' class='btn btn-success' /><input type='reset' value='reset' class='btn btn-danger' /><img id='loading3' style='position:relative; top:10px; display:none' src='image/loading.gif' /></td></tr>";
  
?>
     </table>
  </form> 
  
  <div id="tabel2" style="overflow-x: auto">
  </div><br>
    <button id='mulai' style='display:none' class="btn btn-danger">Reset</button>
</center>
  </div>
  
<?php echo $this->load->view('js'); ?>
</html>
</body>