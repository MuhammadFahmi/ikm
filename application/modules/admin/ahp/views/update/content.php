<?php error_reporting(0);?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <?php echo $this->load->view('css_top')?>
</head>
<body>
      <!-- loaders -->
        <div class="loaders"></div>
      <!-- /loaders -->

    <div class="x_title">
      <h2>Form Update Responden <small>sub title</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>

      <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php if($this->session->flashdata('login_error')){?>
    <div class="alert alert-success">success</div>    
    <?php }else{?>
    <!-- <div class="alert alert-success"></div>     -->
     <?php } ?>
    <div class="notif"></div>

      <form class="form-horizontal form-label-left" novalidate>

      <?php $query = core::get_where('responden','gammu',array('id' => $this->uri->segment(3)),1); ?>
      <?php $row = $query->row_array();
        // print_r($row);
      ?>

        <input type="hidden" class="id_r" value=<?php echo $row['id'];?> >
        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Lengkap <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="nama" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="nama" placeholder="Enter......" required="required" type="text"  value="<?php echo $row['nama_responden']?>">
          </div>
        </div>

        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tempat_lahir">Tempat Lahir <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="tempat_lahir" class="form-control col-md-7 col-xs-12" name="tempat_lahir" placeholder="Enter......" required="required" type="text"  value="<?php echo $row['tempat_lahir']?>">
          </div>
        </div>

        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="birthday">Tanggal Lahir <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="birthday" class="form-control col-md-7 col-xs-12" required="required" type="text" placeholder="Enter......"  value="<?php echo $row['tanggal_lahir']?>">
          </div>
        </div>

        <script type="text/javascript">
          $(document).ready(function() {
            $('#birthday').daterangepicker({
            // viewMode: "months", 
            // minViewMode: "months",
              singleDatePicker: true,
              showDropdowns: true,
              yearRange:"-100:+0",
              calender_style: "picker_1",
            }, 
             function(start, end, label) {
               var years = moment().diff(start, 'years');
               // alert("Usia Anda " + years + " Tahun.");
              console.log(start.toISOString(), end.toISOString(), label);
              var usia = $("#usia").val(years + " Tahun");

            });
          });
        </script>

        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usia">Usia <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="usia" class="form-control col-md-7 col-xs-12" name="usia" placeholder="Enter......"required="required" type="text" disabled="true" value="<?php echo $row['usia']?>">
          </div>
        </div>

        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Alamat <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea id="alamat" required="required" name="alamat" class="form-control col-md-7 col-xs-12"><?php echo $row['alamat']?></textarea>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin<span class="required">*</span></label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <p style="margin-top: 10px">
            <input type="radio" class="flat" name="jenis_kelamin" id="genderL" value="Laki-Laki" <?php 
              if($row['jenis_kelamin'] == "Laki-Laki")
              {
                echo "checked";
              }
              ?> />
              Laki-Laki &nbsp;
             <input type="radio" class="flat" name="jenis_kelamin" id="genderP" value="Perempuan" 
             <?php 
              if($row['jenis_kelamin'] == "Perempuan")
              {
                echo "checked";
              }
              ?> />
             Perempuan    
          </p>
          </div>
        </div>

        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="select">Pendidikan Terakhir<span class="required">*</span></label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <select class="select2_single form-control" tabindex="-1" style="width: 240px;" id="single1" name="single1">
              <option value="empty">Pilih Pendidikan Terakhir</option> 
              <option value="SD"
              <?php 
                if($row['pendidikan_terakhir']=="SD")
                {
                  echo "selected=selected";
                }
              ?>>SD ke bawah</option>
              <option value="SLTP"
              <?php 
                if($row['pendidikan_terakhir']=="SLTP")
                {
                  echo "selected=selected";
                }
              ?>>SLTP</option>
              <option value="SLTA"
              <?php 
                if($row['pendidikan_terakhir']=="SLTA")
                {
                  echo "selected=selected";
                }
              ?>>SLTA</option>
              <option value="D1-D3-D4" 
              <?php 
                if($row['pendidikan_terakhir']=="D1-D3-D4")
                {
                  echo "selected=selected";
                }
              ?>>D1-D3-D4</option>
              <option value="S1" 
              <?php 
                if($row['pendidikan_terakhir']=="S1")
                {
                  echo "selected=selected";
                }
              ?>>S1</option>
              <option value="S2"
              <?php 
                if($row['pendidikan_terakhir']=="S2")
                {
                  echo "selected=selected";
                }
              ?>>S2 ke atas</option>
            </select>
          </div>
        </div>

        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Pekerjaan Utama<span class="required">*</span></label>
          <div class="col-md-6 col-sm-6 col-xs-12">
      
        <p style="padding: 5px;">
          <input type="checkbox" name="hobbies[]" id="hobby1" value="PNS" data-parsley-mincheck="2"  class="flat" 
          <?php 
              if($row['pekerjaan']=="PNS")
              {
                echo "checked";
              }
          ?>/> PNS
          <br />

          <input type="checkbox" name="hobbies[]" id="hobby2" value="TNI" class="flat" 
          <?php 
              if($row['pekerjaan']=="TNI")
              {
                echo "checked";
              }
          ?>/> TNI
          <br />

          <input type="checkbox" name="hobbies[]" id="hobby3" value="POLRI" class="flat" 
          <?php 
              if($row['pekerjaan']=="POLRI")
              {
                echo "checked";
              }
          ?>/> POLRI
          <br />

          <input type="checkbox" name="hobbies[]" id="hobby4" value="Wiraswasta" class="flat" 
          <?php 
              if($row['pekerjaan']=="Wiraswasta")
              {
                echo "checked";
              }
          ?>/> Wiraswasta
          <br />

          <input type="checkbox" name="hobbies[]" id="hobby5" value="Buruh" class="flat" 
          <?php 
              if($row['pekerjaan']=="Buruh")
              {
                echo "checked";
              }
          ?>/> Buruh
          <br />

          <input type="checkbox" name="hobbies[]" id="hobby6" value="Lainnya" class="flat" 
          <?php 
              if($row['pekerjaan']=="Lainnya")
              {
                echo "checked";
              }
          ?>/> Lainnya
          <br />

          <p>
          </div>
        </div>

        <style type="text/css">
          /*box-shadow: 10px 1px 5px #888888;*/
        .loader{
          margin-left: 150px;
          border: 1px solid transparant;
          border-radius: 3px;
          box-sizing: 10px;
          box-shadow:inset 0 0 1em gold, 0 0 1em red;
          width: 220px;
          height: 100px;
          background-color: white;
          z-index: 3000;
          position: absolute;
          display: none;
        }
        .loader img{
          padding: 10px;
          height: 60px;
          width: 60px;
          color:white;
        }
        .loader p{
          /*color:white;*/
        }
        .overlay .loader{
          background-color: white;
          opacity: 0.9;
          z-index: 1;
          position: absolute;
        }

        </style>
        
        <div class="loader" style="">
        <center>
        <img src="<?php echo base_url()?>assets/loaders/indicator-big.gif" style=""><p></p>
        </center>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Sub Unit<span class="required">*</span></label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <select class="select2_single form-control" tabindex="-1" style="width: 240px;" id="single2">
              <option value="empty">Pilih Sub Unit</option>
              <?php foreach ($sub_unit->result_array() as $key) { ?>                           
              <option value="<?php echo $key['id']?>" 
                <?php 
                if($row['id_subunit']==$key['id'])
                {
                  echo "selected=selected";
                }
            ?>> <?php echo $key['sub_unit'] ?>
              </option>
              <?php } ?>
            </select>
          </div>
        </div>

        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value=<?php echo $row['email'] ?>>
          </div>
        </div>

        <div class="item form-group">
          <label for="password" class="control-label col-md-3">Password</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="password" type="password" name="password" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required="required" placeholder="Enter......" value=<?php echo $row['password'] ?> >
          </div>
        </div>

        <div class="item form-group">
          <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Ulangi Password</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="password2" type="password" name="password2" data-validate-linked="password" class="form-control col-md-7 col-xs-12" required="required" placeholder="Enter......" value=<?php echo $row['password'] ?>>
          </div>
        </div>

        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Telephone <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="tel" id="telephone" name="phone" required="required" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12" placeholder="Enter......" value=<?php echo $row['telepon'] ?>>
          </div>
        </div>
        
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-md-offset-3">
            <button type="submit" class="btn btn-primary">Cancel</button>
            <button id="send" type="submit" class="btn btn-success">Update</button>
          </div>
        </div>
      </form>
<?php echo $this->load->view('js'); ?>
</html>
</body>