<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kuesioner extends Admincore
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('kuesioner_model','model');
    }
    
    /* METHOD "READ"
     * berfungsi untuk membaca query dari database dengan system pagination
     */
    function index()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['kuesioner'] = core::getAll("kuesioner","gammu");
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
       
     /* METHOD "SEARCH"*/
    function search()
    {
        $tanggal =  date("Y-m-d",strtotime( $_GET['tanggal']));
        $jadwal = core::getWhere("jadwal","gammu", array(
            'tanggal'=>$tanggal
        ));
        $row = $jadwal->row_array();
        if($jadwal->num_rows() > 0){
               $data['join'] = core::manualQuery("
                    select u.*,s.id_jadwal,d.*, SUM(d.skor) as skor, COUNT(type) AS type from survey s INNER JOIN detail_survey d on d.id_survey = s.id INNER JOIN unsur u on u.id_pertanyaan = d.nomor_soal where s.id_jadwal = '$row[id]' GROUP BY d.nomor_soal","gammu"
                    );         

                $data['level'] = $this->session->userdata('level_id');
                $data['email'] = $this->session->userdata('email');
                $data['unsur'] = core::getAll("unsur","gammu");
                $data['include'] =   $this->load->view('/search/include','',TRUE);
                $data['content'] =   $this->load->view('/search/content',$data,TRUE);
                $this->load->view("kuesioner/search/content");
        }else{
                $data['level'] = $this->session->userdata('level_id');
                $data['email'] = $this->session->userdata('email');
                $data['unsur'] = core::getAll("unsur","gammu");
                $data['include'] =   $this->load->view('/search/include','',TRUE);
                $data['content'] =   $this->load->view('/search/content',$data,TRUE);
                $this->load->view("kuesioner/search/content");
        }
    }   
   
    function searchNrr()
    {
        $tanggal =  date("Y-m-d",strtotime( $_GET['tanggal']));
        $jadwal = core::getWhere("jadwal","gammu", array(
            'tanggal'=>$tanggal
        ));
        $row = $jadwal->row_array();
        if($jadwal->num_rows() > 0){
               $data['join'] = core::manualQuery("
                    select u.*,s.id_jadwal,d.*, SUM(d.skor) as skor, COUNT(type) AS type from survey s INNER JOIN detail_survey d on d.id_survey = s.id INNER JOIN unsur u on u.id_pertanyaan = d.nomor_soal where s.id_jadwal = '$row[id]' GROUP BY d.nomor_soal","gammu"
                    );         

                $data['level'] = $this->session->userdata('level_id');
                $data['email'] = $this->session->userdata('email');
                $data['unsur'] = core::getAll("unsur","gammu");
                $data['include'] =   $this->load->view('/search/include','',TRUE);
                $data['content'] =   $this->load->view('/search/contents',$data,TRUE);
                $this->load->view("kuesioner/search/contents");
        }
        else{
                // $data['level'] = $this->session->userdata('level_id');
                // $data['email'] = $this->session->userdata('email');
                // $data['unsur'] = core::getAll("unsur","gammu");
                // $data['include'] =   $this->load->view('/search/include','',TRUE);
                // $data['content'] =   $this->load->view('/search/content',$data,TRUE);
                // $this->load->view("kuesioner/search/content");
        }
    }   
    
    function create()
    {
        $this->load->library('session');
       if(isset($_REQUEST['jsonObj']))
       {
            $data = $this->input->post('jsonObj');
            foreach ($data as $key) {
            // variable untuk menampung data Pengguna
            core::insert('kuesioner','gammu',array(
                'pertanyaan' => $key['pertanyaan'],
                'A' => $key['A'],
                'B' => $key['B'],
                'C' => $key['C'],
                'D' => $key['D'],
            ));
            }
       }
       else{
            $data['level'] = $this->session->userdata('level_id');
            $data['email'] = $this->session->userdata('email');
            $data['persepsi'] = core::getAll('nilai_persepsi','gammu'); 
            $data['include']  = $this->load->view('/create/include','',TRUE);
            $data['content']  = $this->load->view('/create/content',$data,TRUE);
            $this->load->view('/admin/main',$data);
       }
    }

    /* METHOD "UPDATE"
     * berfungsi untuk mengupdate data dari database
     */
    function update($id = '')
    {
        
        if(isset($_GET['kode_data']))
        {
            $data = explode("|",isset($_GET['kode_data']) ? $_GET['kode_data'] : "");
            // print_r($data);
            core::update('kuesioner','gammu',array(
                'pertanyaan' => $data[1],
                'A' => $data[2],
                'B' => $data[3],
                'C' => $data[4],
                'D' => $data[5],
            ), $data[0]); 
       }
       else{
            $data['level'] = $this->session->userdata('level_id');
            $data['email'] = $this->session->userdata('email');
            $data['kuesioner'] = core::getAll('kuesioner','gammu'); 
            $data['include'] =   $this->load->view('/update/include','',TRUE);
            $data['content'] =   $this->load->view('/update/content',$data,TRUE);
            $this->load->view("admin/main",$data);
       }
    }
    
    function detail($id = '')
    {
        $data['include'] =   $this->load->view('/detail/include','',TRUE);
        $data['content'] =   $this->load->view('/detail/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
    // ACTIONS METHOD
    // method-method yang berfungsi hanya untuk sebuah actions/tidak ada view
    
    /* METHOD "DELETE"
     * berfungsi untuk menghapus data dari database
     */
    function delete($id = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters('<p class="text-alert">', '</p> ');
        core::delete('kuesioner','gammu','id',$id);     
        $this->session->set_flashdata('success','success');
        redirect('kuesioner');
    }

    function submited()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['jadwal']  = core::getAll("jadwal","gammu");
        $data['include'] = $this->load->view('/read/include','',TRUE);
        $data['content'] = $this->load->view('/read/submited',$data,TRUE);
        $this->load->view("admin/main",$data);
    }

    function insertSurvey()
    {
        if($this->session->userdata('email') != ""){
            $tanggal  = $this->uri->segment(3);
            $email    = $this->session->userdata('email');
            $_username= core::getWhere("responden","gammu",array("email"=>$email))->result();
            foreach ($_username as $key) {
                $username = $key->nama_responden;
            }
            $_tanggal = core::getWhere("jadwal","gammu",array("tanggal"=>$tanggal));
            foreach ($_tanggal->result() as $key) {
                $id_jadwal =$key->id; 
            }
            $insert = core::insert("survey","gammu",array(
                "id_jadwal"=>$id_jadwal,
                "nama_responden"=>$username,
                "email"=>$email,
                "total_nilai"=>0,
            ));
            // $this->isiKuesioner();
            redirect("kuesioner/loadKuesioner");
        }else{
            
        }
    }
    
    function loadKuesioner()
    {
        $email = $this->session->userdata('email');
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['query'] = core::getMax("survey","gammu",$email);
        foreach ($data['query']->result() as $key) {
            $data['id_max'] =  $key->id;
        }
        if($this->uri->segment(3) == "loadKuesioner")
        {
           
        }
        else{
            $per_page = 5;
            $segment  = 3;
            $url      = 'kuesioner/loadKuesioner';
        }   
        $data['kuesioner'] = core::get_all_pagination("kuesioner","gammu",$per_page,$segment,$url);
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/loadKuesioner',$data,TRUE);
        $this->load->view("admin/main",$data);                
    }

    function simpanJawaban()
    {
        $data = $_POST['jsonObj'];
        if($data['tipe'] == "A"){
            $skor = 1;
        }
        else if($data['tipe'] == "B"){
            $skor = 2;
        }
        else if($data['tipe'] == "C"){
            $skor = 3;
        }
        else{
            $skor = 4;
        }
        $list = core::getWhere("detail_survey","gammu",array(
            'id_survey'=>$data['id_survey'],
            'nomor_soal'=>$data['nomor_soal']
        ));
        foreach ($list->result() as $key) {
            $id =  $key->id;
         } 
        if($list->num_rows() > 0 )
        {
           // echo "update";
            core::update_where("detail_survey","gammu", array(
                'id_survey' =>$data['id_survey'],
                'nomor_soal'=>$data['nomor_soal'],
                'jawaban'   =>$data['jawaban'],
                'type'      =>$data['tipe'],
                'skor'      =>$skor
                ),  
                'id',$id,
                'nomor_soal',$data['nomor_soal'], 
                'id_survey',$data['id_survey']
                );
        }
        else{
           // echo "insert";
            core::insert("detail_survey","gammu",array(
                'id_survey' =>$data['id_survey'],
                'nomor_soal'=>$data['nomor_soal'],
                'jawaban'   =>$data['jawaban'],
                'type'      =>$data['tipe'],
                'skor'      =>$skor
               ));
        }
    }

    function hitungIkm()
    {
        echo "terima kasih";
    }

    function hasilSurvey()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/hasilSurvey',$data,TRUE);
        $this->load->view("admin/main",$data);
    }

    function laporanGrafik()
    {
        $this->load->helper('time_helper');
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/laporanGrafik',$data,TRUE);
        $this->load->view("admin/main",$data);   
    }

    function laporanBulananExcel()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/laporanBulananExcel',$data,TRUE);
        $this->load->view("admin/main",$data);
    }

    function laporanBulanan()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/laporanBulanan',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
    
    function cekBaris($query,$querys,$bulan,$tahun,$pdfFilePath)
    {
        $data = array();
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
        $data['join']  = $query;
        $data['joins'] = $querys;
        if($query->num_rows() != 0){
            $html = $this->load->view('read/pdf_report', $data, true); // render the view into HTML

            ini_set('memory_limit','32M');
            include_once APPPATH.'/third_party/mpdf/mpdf.php';
            $param = '"en-GB-x","A4-P","","",10,10,10,10,6,3'; 
            $pdf = new mPDF();
            $pdf->AddPage('P','','','','',10,10,2,10,6,3);   
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'F'); // save to file because we can
            
            $this->load->helper('download');
            $data = file_get_contents($pdfFilePath); // Read the file's contents
            $name = 'laporanBulanan_'.date('dMy_h_s_i').'.pdf';
            force_download($name, $data);
        }else{
            echo "<script>alert('Laporan untuk bulan $bulan $tahun tidak ada')</script>";
            echo "<script>window.location = 'kuesioner/laporanBulanan'</script>";
        }
        return $data;           
    }

    function cetakPdf()
    {
        $this->load->helper('time_helper');
        $data = array();
        $data['bulan'] = bulan($_REQUEST['bulan']);         
        $data['tahun'] = $_REQUEST['tahun'];         
        if($_REQUEST['filter'] == "semua"){
            
            $pdfFilePath = FCPATH.'/laporan/pdf/Bulanan/laporanBulanan_'.date('dMy_h_s_i').'.pdf';
            
            $data['join'] =  
                core::hitungNrr(
                    'survey s', 'detail_survey d', 'unsur u', 'jadwal j','d.id_survey = s.id', 'u.id_pertanyaan = d.nomor_soal', 'j.id = s.id_jadwal','gammu', array('month(j.tanggal)'=>$_REQUEST['bulan'], 'year(j.tanggal)'=>$_REQUEST['tahun']));
            
            
            $data['joins'] =  core::manualQuery("
                        select su.sub_unit, COUNT(s.email) AS jml, j.tanggal from survey s 
                        INNER JOIN jadwal j on j.id = s.id_jadwal 
                        INNER JOIN responden r on r.email = s.email 
                        INNER JOIN sub_unit su on su.id = r.id_subunit 
                        where month(j.tanggal) = '$_REQUEST[bulan]' and year(j.tanggal) = '$_REQUEST[tahun]'  GROUP BY su.sub_unit","gammu"
                        );
            $this->cekBaris($data['join'],$data['joins'],$data['bulan'],$data['tahun'],$pdfFilePath);
            
        }else if($_REQUEST['filter'] == "empty"){
            echo "<script>alert('filter belum dipilih')</script>";
            echo "<script>window.location = 'kuesioner/laporanBulanan'</script>";
        }else{
            $pdfFilePath = FCPATH.'/laporan/pdf/Bulanan_Per_sub_Unit/laporanBulanan_'.date('dMy_h_s_i').'.pdf';
            
            $data['join'] =  core::manualQuery("
                        select su.sub_unit, u.*,s.id_jadwal,d.*, SUM(d.skor) as skor, COUNT(type) AS type, j.tanggal from survey s 
                        INNER JOIN detail_survey d on d.id_survey = s.id 
                        INNER JOIN unsur u on u.id_pertanyaan = d.nomor_soal 
                        INNER JOIN jadwal j on j.id = s.id_jadwal 
                        INNER JOIN responden r on r.email = s.email 
                        INNER JOIN sub_unit su on su.id = r.id_subunit 
                        where month(j.tanggal) = '$_REQUEST[bulan]' and year(j.tanggal) = '$_REQUEST[tahun]' and su.id = '$_REQUEST[filter]' GROUP BY d.nomor_soal ","gammu"
                        );
            
            $data['joins'] =  core::manualQuery("
                        select su.sub_unit, COUNT(s.email) AS jml, j.tanggal from survey s 
                        INNER JOIN jadwal j on j.id = s.id_jadwal 
                        INNER JOIN responden r on r.email = s.email 
                        INNER JOIN sub_unit su on su.id = r.id_subunit 
                        where month(j.tanggal) = '$_REQUEST[bulan]' and year(j.tanggal) = '$_REQUEST[tahun]' and su.id = '$_REQUEST[filter]' GROUP BY su.sub_unit","gammu"
                        );

            $this->cekBaris($data['join'],$data['joins'],$data['bulan'],$data['tahun'],$pdfFilePath);              
        }
    }

    function cekBarisExcel($query,$querys,$bulan,$tahun,$filter)
    {
        $data = array();
        $data['join'] = $query;
        $data['joins'] = $querys;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
        foreach ($data['joins']->result() as $key) {
            $lokasi = $key->Lokasi;
            $Jumlah_Sampel = $key->Jumlah_Sampel;
            $koordinator = $key->koordinator;
            $alamat = $key->alamat;
        }
        if($query->num_rows() !=0)
        {
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Laporan Bulanan")->setDescription("none");
 
            $objPHPExcel->setActiveSheetIndex(0);
            // Field names in the first row
            
            $sheet = $objPHPExcel->getActiveSheet();
            // Border in the table
            $objPHPExcel->getActiveSheet(0)->getStyle('A4:F20')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            
            $objPHPExcel->getSheet(0)->getStyle('A4:F4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getSheet(0)->getStyle('A4:F4')->getFill()->getStartColor()->setRGB('D0D0D0');
            $objPHPExcel->getSheet(0)->getStyle('A23:B23')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getSheet(0)->getStyle('A23:B23')->getFill()->getStartColor()->setRGB('D0D0D0');
            $objPHPExcel->getSheet(0)->getStyle('A37:D37')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getSheet(0)->getStyle('A37:D37')->getFill()->getStartColor()->setRGB('D0D0D0');
            
            $sheet->getStyle('A4:F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $styleArray = array(
             'font' => array(
             // 'underline' => PHPExcel_Style_Font::bold_SINGLE,
             'name' => 'Arial',
             'size' => 18,
             // 'bold' => true
             )
            );
            $sheet->mergeCells('A1:F1');    
            $sheet->mergeCells('A2:F2');    
            $sheet->setCellValue('F4','Mutu Pelayanan');
            $sheet->setCellValue('A1','Survey Indeks Kepuasan Masyarakat');
            $sheet->setCellValue('A2','Bulan'." ". $data['bulan']." ".$data['tahun']);
            $sheet->getStyle('A1:A2')->applyFromArray($styleArray);
            $sheet->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $fields  = $data['join']->list_fields();
            $fl  = $data['joins']->list_fields();
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 4, strtoupper($field));
                $col++;
            }
            
            // Fetching the table data
            $row = 5;
            $total = 0;
            foreach($data['join']->result() as $data)
            {
                $total += $data->NRR_Tertimbang;
                $col = 0;
                foreach ($fields as $field)
                {
                    if($col == 1){
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);                                     
                    }else{
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);                  
                    }
                    
                    $col++;
                }            
                $row++;
            }
            
            $sheet->mergeCells('A19:D19');    
            $sheet->mergeCells('A20:D20');    
            $sheet->setCellValue('A19','Total NRR');
            $sheet->setCellValue('A20','IKM UNIT PELAYANAN ('.round($total,3).' * 25)');
            $sheet->getStyle('A19:D20')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('C4:F20')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
           
            $sheet->setCellValue('E19', round($total,3));
            $sheet->setCellValue('E20', round($total,3)*25);
            $NilaiIkm = round($total,3)*25;
            if($NilaiIkm>=81.26 && $NilaiIkm<=100.00){
              $grade = "A";
              $ket   = "(Sangat Baik)";
            }
            else if($NilaiIkm>=62.51 && $NilaiIkm<=81.25){
              $grade = "B";              
              $ket   = "(Baik)";
            }
            else if($NilaiIkm>=43.76 && $NilaiIkm<=62.50){
              $grade = "C";              
              $ket   = "(Kurang Baik)";
            }
            else if($NilaiIkm>=25 && $NilaiIkm<=43.75){
              $grade = "D";              
              $ket   = "(Tidak Baik)";
            }
            else{ 
              $grade = "";              
              $ket   = "";
            }

            $objPHPExcel->getActiveSheet(0)->getStyle('A23:B34')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
   
            $sheet->getStyle('A23:B23')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('B29')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            
            $sheet->setCellValue('F20', $grade ." ". $ket);
            $sheet->setCellValue('A23', 'Keterangan');
            $sheet->setCellValue('B23', 'Unsur-Unsur Pelayanan');
            $sheet->setCellValue('A24', 'U01 s/d U14');
            $sheet->setCellValue('B24', 'Unsur-Unsur Pelayanan');
            $sheet->setCellValue('A25', 'NRR');
            $sheet->setCellValue('B25', 'Nilai Rata-Rata');
            $sheet->setCellValue('A26', 'IKM');
            $sheet->setCellValue('B26', 'Indeks Kepuasan Masyarakat');
            $sheet->setCellValue('A27', 'NRR Per Unsur');
            $sheet->setCellValue('B27', 'Jumlah nilai per unsur dibagi jumlah kuesioner yang terisi');
            $sheet->setCellValue('A28', 'NRR Tertimbang');
            $sheet->setCellValue('B28', 'NRR Per Unsur x 0,071');
            $sheet->setCellValue('A29', 'IKM UNIT PELAYANAN');
            $sheet->setCellValue('B29', $NilaiIkm);
            $sheet->setCellValue('A30', 'Mutu Pelayanan');
            $sheet->setCellValue('B30', '');
            $sheet->setCellValue('A31', 'A (Sangat Baik)');
            $sheet->setCellValue('B31', '81,26 - 100,00');
            $sheet->setCellValue('A32', 'B (Baik)');
            $sheet->setCellValue('B32', '62,51 - 81,25');
            $sheet->setCellValue('A33', 'C (Kurang Baik)');
            $sheet->setCellValue('B33', '43,76 - 62,50');
            $sheet->setCellValue('A34', 'D (Tidak Baik)');
            $sheet->setCellValue('B34', '25,00 - 43,75');

            $objPHPExcel->getActiveSheet(0)->getStyle('A37:D39')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);  
            
                $sheet->setCellValue('A37', 'No');
                $sheet->setCellValue('A38', 1);
                $sheet->setCellValue('B37', 'Lokasi');
                $sheet->setCellValue('B38', $lokasi);
                $sheet->setCellValue('C37', 'Jumlah Sampel');
                $sheet->setCellValue('C38', $Jumlah_Sampel);
                $sheet->setCellValue('D37', 'Keterangan');
                $sheet->mergeCells('A39:B39');

            $sheet->getStyle('A39:B39')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->setCellValue('A39', 'Total');
            $sheet->setCellValue('C39', $Jumlah_Sampel);
            
            foreach(range('A','G') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                                              ->setAutoSize(true);             
            }
            
            $objPHPExcel->setActiveSheetIndex(0);
     
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
     
            // Sending headers to force the user to download the file
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="laporanBulananExcel_'.date('dMy_h_s_i').'.xls"');
            header('Cache-Control: max-age=0');
     
            $objWriter->save('php://output');
        }else{
            echo "<script>alert('Laporan untuk bulan $bulan $tahun tidak ada')</script>";
            echo "<script>window.location = 'kuesioner/laporanBulananExcel'</script>";
        }
        return $data;            
    }

    function cetakExcel()
    {
        $data['tahun'] = $_REQUEST['tahun'];         
        $data['bulan'] = bulan($_REQUEST['bulan']);         
        if($_REQUEST['filter'] == "semua"){
        
        $data['join'] =  
            core::manualQuery("
                select  u.id_unsur, u.nama_unsur, SUM(d.skor) as Nilai_Per_Unsur, 
                        round((SUM(d.skor)/COUNT(type)),3) AS NRR_per_unsur, 
                        round((SUM(d.skor)/COUNT(type))*0.071, 3) AS NRR_Tertimbang 
                        from survey s 
                        INNER JOIN detail_survey d on d.id_survey = s.id 
                        INNER JOIN unsur u on u.id_pertanyaan = d.nomor_soal 
                        INNER JOIN jadwal j on j.id = s.id_jadwal 
                        INNER JOIN responden r on r.email = s.email 
                        INNER JOIN sub_unit su on su.id = r.id_subunit 
                        where month(j.tanggal) = '$_REQUEST[bulan]' and year(j.tanggal) = '$_REQUEST[tahun]' GROUP BY d.nomor_soal ","gammu"
                        );

        $data['joins'] =  
            core::manualQuery("
                select su.sub_unit AS Lokasi, COUNT(s.email) AS Jumlah_Sampel, su.koordinator, su.alamat from survey s 
                    INNER JOIN jadwal j on j.id = s.id_jadwal 
                    INNER JOIN responden r on r.email = s.email 
                    INNER JOIN sub_unit su on su.id = r.id_subunit 
                    where month(j.tanggal) = '$_REQUEST[bulan]' and year(j.tanggal) = '$_REQUEST[tahun]' GROUP BY su.sub_unit","gammu"
                    );
               
            $this->cekBarisExcel($data['join'], $data['joins'], $data['bulan'], $data['tahun'], $_REQUEST['filter']);        
        }else if($_REQUEST['filter'] == "empty"){
            echo "<script>alert('filter belum dipilih')</script>";
            echo "<script>window.location = 'kuesioner/laporanBulananExcel'</script>";
        }else{

        $data['join'] =  
            core::manualQuery("
                select  u.id_unsur, u.nama_unsur, SUM(d.skor) as Nilai_Per_Unsur, 
                        round((SUM(d.skor)/COUNT(type)),3) AS NRR_per_unsur, 
                        round((SUM(d.skor)/COUNT(type))*0.071, 3) AS NRR_Tertimbang 
                        from survey s 
                        INNER JOIN detail_survey d on d.id_survey = s.id 
                        INNER JOIN unsur u on u.id_pertanyaan = d.nomor_soal 
                        INNER JOIN jadwal j on j.id = s.id_jadwal 
                        INNER JOIN responden r on r.email = s.email 
                        INNER JOIN sub_unit su on su.id = r.id_subunit 
                        where month(j.tanggal) = '$_REQUEST[bulan]' and year(j.tanggal) = '$_REQUEST[tahun]' and su.id = '$_REQUEST[filter]' GROUP BY d.nomor_soal ","gammu"
                        );

        $data['joins'] =  
            core::manualQuery("
                select su.sub_unit AS Lokasi, COUNT(s.email) AS Jumlah_Sampel, su.koordinator, su.alamat from survey s 
                    INNER JOIN jadwal j on j.id = s.id_jadwal 
                    INNER JOIN responden r on r.email = s.email 
                    INNER JOIN sub_unit su on su.id = r.id_subunit 
                    where month(j.tanggal) = '$_REQUEST[bulan]' and year(j.tanggal) = '$_REQUEST[tahun]' and su.id = '$_REQUEST[filter]' GROUP BY su.sub_unit","gammu"
                    );
               
            $this->cekBarisExcel($data['join'], $data['joins'], $data['bulan'], $data['tahun'], $_REQUEST['filter']);
        }       
    }
}