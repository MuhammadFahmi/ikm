
  <!-- form validation -->
  <script src="<?php echo base_url();?>assets/gantella/js/validator/validator.js"></script>

   <script>
    // $(document).ready(function() {
    // $(".js-example-basic-single").select2();
  // });
    $(document).ready(function() {
      $(".select2_single").select2({
        placeholder: "Select a state",
        allowClear: true
      });
      $(".select2_group").select2({});
      $(".select2_multiple").select2({
        maximumSelectionLength: 4,
        placeholder: "With Max Selection limit 4",
        allowClear: true
      });
    });
  </script>
  <!-- /select2 -->

  <!-- select2 -->
  <script src="<?php echo base_url();?>assets/gantella/js/select/select2.full.js"></script>
       
     <!-- parsely -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/gantella/js/parsley/parsley.min.js"></script>
                  
  
  <script type="text/javascript">
  
   // <!-- form validation -->
    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#demo-form .btn').on('click', function() {
        $('#demo-form').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#demo-form').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });

    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#demo-form2 .btn').on('click', function() {
        $('#demo-form2').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#demo-form2').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });
    try {
      hljs.initHighlightingOnLoad();
    } catch (err) {}
  </script>

  <script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // var jsonObj = $(this).serialize();
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }
      else{
        add_item();
      
      return false;
    }     
  
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);
  </script>
  

  <script type="text/javascript">
        
        $(document).ready(function(){
          $(document).on('click', '#hapus', function(e) {
            var index = $('#hapus').index(this);  
            $('.baris').eq(index).remove();
          });
        });

        function add_item()
        {         
          $(".details").show();
          var pertanyaan = $('#pertanyaan');         
          var A       = $('#A');         
          var B       = $('#B');         
          var C       = $('#C');         
          var D       = $('#D');         
          var count = $('.pk[nilai="'+pertanyaan.val()+'"]').length;

          if (count==0){
            $('#datatable-responsive tbody').append(
              "<tr class='baris'>" +
              "<td style='display:none' class='pk' nilai="+pertanyaan.val()+" >" + pertanyaan.val() + "</td>" +
              "<td class='pertanyaan'>" + "<input type='text' value="+ pertanyaan.val() +" class='form-control' id='isiPertanyaan'>" + "</td>" +
              "<td class='A'>" + "<input type='text' value="+ A.val() +" class='form-control' id='isiA'>" + "</td>" +
              "<td class='B'>" + "<input type='text' value="+ B.val() +" class='form-control' id='isiB'>" + "</td>" +
              "<td class='C'>" + "<input type='text' value="+ C.val() +" class='form-control' id='isiC'>" + "</td>" +
              "<td class='D'>" + "<input type='text' value="+ D.val() +" class='form-control' id='isiD'>" + "</td>" +
              "<td class='hapus'><center><button class='btn btn-danger' id='hapus'>X</button></center></td>" +
              "</tr>"
            );
            pertanyaan.val("").focus;
            A.val("");
            B.val("");
            C.val("");
            D.val("");
          }
          else{
              alert("Pertanyaan sudah ada");  
          }
        }

        function kirim(){
          var jml = $('.baris').length;
          var jsonObj = [];
          var pertanyaan = $('#pertanyaan').val();
          var head = {
            pertanyaan : pertanyaan
          }
          
          $(".baris").each(function() {
          var pertanyaan = $(this).find('#isiPertanyaan').val();
          var A = $(this).find('#isiA').val();
          var B = $(this).find('#isiB').val();
          var C = $(this).find('#isiC').val();
          var D = $(this).find('#isiD').val();
          item = {}
          item ["pertanyaan"] = pertanyaan;
          item ["A"] = A;
          item ["B"] = B;
          item ["C"] = C;
          item ["D"] = D;
          jsonObj.push(item);
          });
             
             $.ajax({
              type : "post",
              url  : "<?php echo base_url('kuesioner/create') ?>", 
              data : {
                jsonObj :jsonObj,
                // head : head
              },
              cache : false,
              // dataType : "json",
              success: function(msg){
                  alert("kuesioner berhasil di simpan");
              },
              error:function(msg){
                alert("error js");
                // alert('Harus Diisi!!');
              }
            });
          }
        </script>

        <script type="text/javascript">
          $(function(){
            $("#send").click(function(){
              document.location = "<?php echo base_url('kuesioner')?>"; 
            });
          });

        </script>