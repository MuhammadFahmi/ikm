<?php error_reporting(0);?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <?php echo $this->load->view('config/css')?>
</head>
<body>
    <!-- loaders -->
      <div class="loaders"></div>
    <!-- /loaders -->                    
    <div class="x_title">
      <h2>Data Hasil Survey <small>Users</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a href="#"><i class="fa fa-close"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    
    <div class="x_content">
      <center><label style="font-size: 20px">Tanggal Survey<span class="required">*</span></label></center> 
      
      <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cari"><span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input id="tanggal_survey" class="form-control col-md-7 col-xs-12" required="required" type="text" placeholder="Cari ...." value="<?php echo $_GET['tanggal_survey']?>">
        </div>
          <button class="btn btn-primary" id="cari"><i class="fa fa-search"></i> Cari</button>
      </form>
      <form method="get">
      
      </div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#tanggal_survey').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                yearRange:"-100:+0",
                calender_style: "picker_1",
              }, 
               function(start, end, label) {
                 var years = moment().diff(start, 'years');
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>
      <div class="detail_survey">
        
      </div>          

      <script type="text/javascript">
        $(document).ready(function(){
          $("#cari").click(function(){
            var tanggal_survey = $("#tanggal_survey").val();
            if(tanggal_survey == ""){
              tanggal_survey.required();
            }
            else{ 
            $.ajax({
              type : "get",
              url  : "<?php echo base_url()?>kuesioner/search",
              data : "tanggal="+tanggal_survey,

              success:function(msg){
                $('.detail_survey').html(msg);
                // alert(msg);
              },
              error:function(msg){
                alert("error");
              
              },
            });
            }
          });
        });

      </script>
    
<?php echo $this->load->view('config/include'); ?>
</html>
</body>