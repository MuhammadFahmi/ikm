<?php include("menuLaporan.php") ?>
      <form method="post" action="<?php echo base_url('kuesioner/cetakexcel')?>" charset='UTF-8' class="form-horizontal form-label-left" style="margin-top: 20px" novalidate>

        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="select">Bulan<span class="required">*</span></label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <select class="select2_single form-control" tabindex="-1" style="width: 240px;" id="single1" name="bulan" required="">
                <option value="empty">Pilih Bulan</option>
                <?php for ($i=1; $i <= count($bulan) ; $i++) {  
                  echo "<option value='$i'>$bulan[$i]</option>";
                } ?>
              </select>
            </div>
          </div>

          <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="select">Tahun<span class="required">*</span></label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <select class="select2_single form-control" tabindex="-1" style="width: 240px;" id="single1" name="tahun" required="">
                <option value="empty">Pilih Tahun</option>
                <?php for ($i=2016; $i <= 2050 ; $i++) {  
                  echo "<option value='$i'>$i</option>";
                } ?>
              </select>
            </div>
          </div>

          <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="select">Filter<span class="required">*</span></label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <select class="select2_single form-control" tabindex="-1" style="width: 240px;" id="single1" name="filter" required="">
                <option value="empty">Pilih Filter</option>
                <option value="semua">Semua</option>
                <?php foreach (core::getAll("sub_unit","gammu")->result() as $key) {  
                  echo "<option value='$key->id'>$key->sub_unit</option>";
                } ?>
              </select>
            </div>
          </div>


          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
              <button id="cari" name="cari" type="submit" class="btn btn-primary"><i class="fa fa-print"></i> unduh Excel</button>
            </div>
          </div>
      </form>