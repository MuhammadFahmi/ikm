<?php error_reporting(0);?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <?php //echo $this->load->view('config/css')?>
</head>
<body>
    <!-- loaders -->
      <div class="loaders"></div>
    <!-- /loaders -->
    <div class="x_title">
      <h2>Laporan IKM<small>Users</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a href="#"><i class="fa fa-close"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <style type="text/css">
    .header_act{
      margin-left: -40px
    }
    .header_act ul li{
      margin: 0px 10px 0px 0px;
      list-style: none;
      display: inline;
    }
    .header_act ul li a{
      color:darkgreen;
      text-decoration: none;
    }
    .header_act ul li a:hover{
      color:red;
    }
    </style>
    <?php 
      $bulan = 
        array(
        '1'=>'Januari',
        '2'=>'Februari',
        '3'=>'Maret',
        '4'=>'April',
        '5'=>'Mei',
        '6'=>'Juni',
        '7'=>'Juli',
        '8'=>'Agustus',
        '9'=>'September',
        '10'=>'Oktober',
        '11'=>'November',
        '12'=>'Desember',
        )?>    
    <div class="x_content">
      <div class="header_act">
        <ul>
          <li><a href="<?php echo base_url('kuesioner/laporanBulanan')?>"><i class="fa fa-file-pdf-o"></i> unduh file pdf</a></li>
          <li><a href="<?php echo base_url('kuesioner/laporanBulananExcel')?>"><i class="fa fa-file-excel-o"></i> unduh file excel</a></li>
          <li><a href="<?php echo base_url('kuesioner/laporanGrafik')?>"><i class="fa fa-bar-chart-o"></i> grafik</a></li>
        </ul>
      </div>

<?php echo $this->load->view('js'); ?>
</html> 
</body>