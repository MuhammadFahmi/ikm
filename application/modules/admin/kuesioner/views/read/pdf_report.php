<img style="width: 645px; height: 189px;" alt="" src="<?php echo base_url();?>laporan/logo.png" alt="" width="637" height="194"> <br>
<style type="text/css">
	table{
		border-collapse: collapse;
		text-align: center;
	}
</style>
   		<h3 align="center">Survey Indeks Kepuasan Masyarakat</h3>
   		<h3 align="center">Bulan <?php echo $bulan." ". $tahun?> </h3>
	    <table border="1" id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead >
          <tr class="headings" style="background-color: #D0D0D0">
            <th>id</th>
            <th>Unsur</th>
            <th>Nilai/unsur</th>
            <th>NRR/unsur</th>
            <th>NRR tertimbang</th>
            <th>Mutu Pelayanan</th>
          </tr>
        </thead>
       
        <tbody style="color: ">
          <?php 
            $total_nrr = 0 ;
            // foreach ($unsur->result() as $key ) {
              foreach ($join->result() as $row) {
                $NRR_Per_Unsur  = $row->skor / $row->type; 
                $NRR_Tertimbang = $NRR_Per_Unsur*0.071 ; 
            ?>
            <tr>
              <td><?php echo $row->id_unsur?></td>
              <td style="text-align: left"><?php echo $row->nama_unsur?></td>
              <td><?php echo $row->skor?></td>
              <td><?php echo round( $NRR_Per_Unsur,3)?></td>
              <td><?php echo round( $NRR_Tertimbang,3)?></td>
              <td><?php ?></td>
          </tr>
              <?php $total_nrr += $NRR_Tertimbang; ?> 
          <?php } ?>
        </tbody>
        <tr>
          <td colspan="4"><center>Total NRR</center></td>
          <td><?php echo round($total_nrr,3)?></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td colspan="4"><center><b>IKM UNIT PELAYANAN (<?php echo round($total_nrr,3)." * "."25"?>)</b></center></td>
          <td><b><?php echo $NilaiIkm = round($total_nrr,3)*25?></b></td>
          <?php 
            if($NilaiIkm>=81.26 && $NilaiIkm<=100.00)
            {
              $grade = "A";
              $ket   = "(Sangat Baik)";
            }
            else if($NilaiIkm>=62.51 && $NilaiIkm<=81.25)
            {
              $grade = "B";              
              $ket   = "(Baik)";
            }
            else if($NilaiIkm>=43.76 && $NilaiIkm<=62.50)
            {
              $grade = "C";              
              $ket   = "(Kurang Baik)";
            }
            else if($NilaiIkm>=25 && $NilaiIkm<=43.75)
            {
              $grade = "D";              
              $ket   = "(Tidak Baik)";
            }
            else{ 
              $grade = "";              
              $ket   = "";

            }
          ?>
          <td><?php echo "<b>".$grade ." ". $ket."</b>" ;?></td>
          <td></td>
        </tr>
      </table>
      <h5>LAMPIRAN 1.KETERANGAN 14 UNSUR PELAYANAN</h5>
      <table border="1" id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
          <tr class="headings" style="background-color: #D0D0D0">
            <th>Keterangan</th>
            <th>Unsur-Unsur Pelayanan</th>
          </tr>
        </thead>
       
        <tbody style="color: ">
          <tr>
              <td align="left">U01 s/d U14</td>
              <td align="left">: Unsur-Unsur Pelayanan</td>
          </tr>
          <tr>
              <td align="left">NRR</td>
              <td align="left">: Nilai Rata-Rata</td>
          </tr>
          <tr>
              <td align="left">IKM</td>
              <td align="left">: Indeks Kepuasan Masyarakat</td>
          </tr>
          <tr>
              <td align="left">NRR Per Unsur</td>
              <td align="left">: Jumlah nilai per unsur dibagi jumlah kuesioner yang terisi</td>
          </tr>
          <tr>
              <td align="left">NRR Tertimbang</td>
              <td align="left">: NRR Per Unsur x 0,071</td>
          </tr>
          <tr>
              <td align="left"><b>IKM UNIT PELAYANAN</b></td>
              <td align="left"><b>: <?php echo $NilaiIkm = round($total_nrr,3)*25?></b></td>
          </tr>
          <tr>
              <td align="left"><b>Mutu Pelayanan</b></td>
              <td></td>
          </tr>
          <tr>
              <td align="left"><b>A</b> (Sangat Baik)</td>
              <td align="left">: 81,26 - 100,00</td>
          </tr>
          <tr>
              <td align="left"><b>B</b> (Baik)</td>
              <td align="left">: 62,51 - 81,25</td>
          </tr>
          <tr>
              <td align="left"><b>C</b> (Kurang Baik)</td>
              <td align="left">: 43,76 - 62,50</td>
          </tr>
          <tr>
              <td align="left"><b>D</b> (Tidak Baik)</td>
              <td align="left">: 25,00 - 43,75</td>
          </tr>
        </tbody>
      </table>
      <h5 style="margin-top: 200px">LAMPIRAN 2.DAFTAR SURVEY INDEKS KEPUASAN MASYARAKAT (IKM)</h5>
      <table border="1" id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
          <tr class="headings" style="background-color: #D0D0D0">
            <th>No</th>
            <th>Lokasi</th>
            <th>Jumlah Sampel</th>
            <th>Keterangan</th>
          </tr>
        </thead>
       
        <tbody style="color: ">
          <?php $no=1;$total=0;foreach ($joins->result() as $rows) { ?>
          <tr>
              <td align="left"><?php echo $no++?></td>
              <td align="left"><?php echo $rows->sub_unit?></td>
              <td align="center"><?php echo $rows->jml?></td>
              <td align="left"></td>
          	<?php $total += $rows->jml?>
          </tr>
          <?php } ?>
         </tbody>
         <tr>
         	<td></td>
         	<td>JUMLAH</td>
         	<td><?php echo $total?></td>
         	<td></td>
         </tr>
      </table>
      <div style="margin-top: 80px;position: relative;line-height: 5px">
      <h5><p style="margin-left: 550px">Bandung <?php echo date("d")." ".$bulan." ".$tahun?></p>
      <p style="margin-left: 540px">Kepala Balai Pengembangan</p><p style="margin-left: 590px"> Perindustrian</p> </h5>
      <br><br>
      <h5><p style="margin-left: 530px; text-decoration: underline;font-weight: bold">Hj. Ellyn Setyairianti, SH. MM</p>
      <p style="margin-left: 590px">Pembina Tk.I</p>
	  <p style="margin-left: 540px">NIP. 195607181976061001</p></h5>
      </div>