<?php error_reporting(0);?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <?php echo $this->load->view('config/css')?>
</head>
<body>
    <!-- loaders -->
      <div class="loaders"></div>
    <!-- /loaders -->                    
    <div class="x_title">
      <h2>Detail Jadwal<small>Users</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a href="#"><i class="fa fa-close"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    
    <div class="x_content">
       <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
          <tr class="headings">
            <th>No</th>
            <th>Tanggal</th>
            <th>Mulai</th>
            <th>Selesai</th>
            <th>Durasi</th>
            <th>Operasi</th>
          </tr>
        </thead>
       
        <tbody style="color: ">
          <?php 
            $no =1;
            foreach ($jadwal->result() as $key ) {
            ?>
            <tr>
              <td><?php echo $no++?></td>
              <td><?php echo $key->tanggal?></td>
              <td><?php echo $key->start_time?></td>
              <td><?php echo $key->doe_time?></td>
              <td><?php echo $key->durasi?></td>
              <td>
              <?php 
                $tanggal     = date("Y-m-d",strtotime($key->tanggal));
                $start_time  = date("H:i",strtotime(substr($key->start_time, "0","5")));
                $doe_time    = date("H:i",strtotime(substr($key->doe_time, "0","5")));
                $tanggal_now = date("Y-m-d");
                $waktuNow    = date("H:i");
                
                if($tanggal== $tanggal_now && $waktuNow >= $start_time && $waktuNow <= $doe_time){?>
                  <a href="<?php echo base_url();?>kuesioner/insertSurvey/<?php echo $key->tanggal?>"><button class="btn btn-primary" name="submit"><i class="fa fa-edit"></i> Mulai Pengisian</button></a>
                
                <?php } else{?>

                  <!-- <a href="<?php //echo base_url();?>kuesioner/insertSurvey/<?php //echo $key->tanggal?>"><button class="btn btn-primary" name="submit"><i class="fa fa-edit"></i> Mulai Pengisian</button></a> -->
                  <button class="btn btn-primary" disabled="true"><i class="fa fa-edit"></i> Mulai Pengisian</button>
                
               <?php } ?>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    
<?php echo $this->load->view('config/include'); ?>
</html>
</body>