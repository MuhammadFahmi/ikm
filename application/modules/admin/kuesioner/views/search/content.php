<?php error_reporting(0)?>
<?php echo $this->load->view('css')?>
<?php if($join){?>
       <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
          <tr class="headings">
            <th>id</th>
            <th>Unsur</th>
            <th>Nilai/unsur</th>
            <th>NRR/unsur</th>
            <th>NRR tertimbang</th>
            <th>Mutu Pelayanan</th>
            <th>Persen</th>
          </tr>
        </thead>
       
        <tbody style="color: ">
          <?php 
            $total_nrr = 0 ;
            // foreach ($unsur->result() as $key ) {
              foreach ($join->result() as $row) {
                $NRR_Per_Unsur  = $row->skor / $row->type; 
                $NRR_Tertimbang = $NRR_Per_Unsur*0.071 ; 
            ?>
            <tr>
              <td><?php echo $row->id_unsur?></td>
              <td><?php echo $row->nama_unsur?></td>
              <td><?php echo $row->skor?></td>
              <td><?php echo round( $NRR_Per_Unsur,3)?></td>
              <td><?php echo round( $NRR_Tertimbang,3)?></td>
              <td><?php ?></td>
              <td><?php ?></td>
          </tr>
              <?php $total_nrr += $NRR_Tertimbang; ?> 
          <?php } ?>
        </tbody>
        <tr>
          <td colspan="4"><center>Total</center></td>
          <td><?php echo round($total_nrr,3)?></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td colspan="4"><center><b>IKM UNIT PELAYANAN (<?php echo round($total_nrr,3)." * "."25"?>)</b></center></td>
          <td><b><?php echo $NilaiIkm = round($total_nrr,3)*25?></b></td>
          <?php 
            if($NilaiIkm>=81.26 && $NilaiIkm<=100.00)
            {
              $grade = "A";
              $ket   = "(Sangat Baik)";
            }
            else if($NilaiIkm>=62.51 && $NilaiIkm<=81.25)
            {
              $grade = "B";              
              $ket   = "(Baik)";
            }
            else if($NilaiIkm>=43.76 && $NilaiIkm<=62.50)
            {
              $grade = "C";              
              $ket   = "(Kurang Baik)";
            }
            else if($NilaiIkm>=25 && $NilaiIkm<=43.75)
            {
              $grade = "D";              
              $ket   = "(Tidak Baik)";
            }
            else{ 
              $grade = "";              
              $ket   = "";

            }
          ?>
          <td><?php echo "<b>".$grade ." ". $ket."</b>" ;?></td>
          <td></td>
        </tr>
      </table>
<?php }else{?>
  <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
          <tr class="headings">
            <th>id</th>
            <th>Unsur</th>
            <th>Nilai/unsur</th>
            <th>NRR/unsur</th>
            <th>NRR tertimbang</th>
            <th>Mutu Pelayanan</th>
            <th>Persen</th>
          </tr>
        </thead>
       
        <tbody style="color: ">
            <tr>
              <td colspan="7" align="center">No data available in table</td>
          </tr>
        </tbody>
      </table>
 <?php } ?>
<?php echo $this->load->view('js')?>
