<?php error_reporting(0)?>
<?php echo $this->load->view('css')?>
<?php if($join){?>
       <table id="datatable-responsive" class="table table-striped table-bordered dt- responsive nowrap" cellspacing="0" width="100%">
       
          <thead style="background-color: #1abb9c;color: white">
            <tr class="headings">
              <?php 
                  foreach ($join->result() as $row) {?>
                  <th><?php echo $row->id_unsur?></th>
              <?php } ?>
            </tr>
          </thead>
          <tbody style="color: ">
            <tr>
              <?php 
              $total_nrr = 0 ;
                foreach ($join->result() as $row) {
                  $NRR_Per_Unsur  = $row->skor / $row->type; 
                  $NRR_Tertimbang = $NRR_Per_Unsur*0.071 ; 
                  ?>
                <td><?php echo round( $NRR_Per_Unsur,3)?></td>
              <?php } ?>
            </tr>
        </tbody>
      </table>
<?php }else{?>
  <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
          <tr class="headings">
            <th>id</th>
            <th>NRR/unsur</th>
          </tr>
        </thead>
       
        <tbody style="color: ">
            <tr>
              <td colspan="7" align="center">No data available in table</td>
          </tr>
        </tbody>
      </table>
 <?php } ?>
<?php echo $this->load->view('js')?>
