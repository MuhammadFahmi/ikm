<?php error_reporting(0)?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <style type="text/css">
  #send{margin-left: 280px;position: relative;top: -49px;}
  #datatable-responsive{position: relative;top: -40px;}
  .loaders {position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: url('../../assets/loaders/ajax-loader1.gif') 50% 50% no-repeat #f2f2f2;opacity: 0.9;filter: alpha(opacity=90);
  }
  </style>
</head>
<body>
      <!-- loaders -->
        <div class="loaders"></div>
      <!-- /loaders -->

      <div class="x_title">
        <h2>Form Update Kuesioner <small>sub title</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>

        <div class="clearfix"></div>
      </div>
      <div class="x_content">
      <div class="notif"></div>

      <?php 
        $query = core::get_wheres('kuesioner','gammu',array('id' => $this->uri->segment(3)),1); 
        $row = $query->row_array();
      ?>

        <form class="form-horizontal form-label-left" novalidate>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Pertanyaan <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea id="pertanyaan" required="required" name="pertanyaan" class="form-control col-md-7 col-xs-12" disabled="true"><?php echo $row['pertanyaan']?></textarea>
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="A">Kalimat A<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="A" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="A" placeholder="Enter......" required="required" type="text" value="<?php echo $row['A']?>" disabled="true">
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="B">Kalimat B<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="B" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="B" placeholder="Enter......" required="required" type="text" value="<?php echo $row['B']?>" disabled="true">
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="C">Kalimat C<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="C" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="C" placeholder="Enter......" required="required" type="text" value="<?php echo $row['C']?>" disabled="true">
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="D">Kalimat D<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="D" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="D" placeholder="Enter......" required="required" type="text" value="<?php echo $row['D']?>" disabled="true">
            </div>
          </div>
                  <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-md-offset-3">
            <button type="submit" class="btn btn-primary" disabled="true"><i class="fa fa-plus"></i> Tambah</button>
          </div>
        </div>
      </form>

      <button id="send" type="submit" class="btn btn-success" onclick="kirim()"><i class="fa fa-send"></i> Update</button>
      
      <style>
      .editbox{display:none}
      td{padding:5px;}
      .editbox{font-size:14px;width:120px;background-color:#fafafa;border:solid 1px #000;border-radius: 3px;padding:4px;}
      .edit_tr:hover{cursor:pointer;}
      </style>

      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
          <tr>
            <th>Pertanyaan</th>
            <th>A</th>
            <th>B</th>
            <th>C</th>
            <th>D</th>
          </tr>
        </thead>
        <tbody style="color: ">
          <tr id="<?php echo $row['id']?>" class="edit_tr">
            <td class="edit_td">
              <span id="pertanyaan_<?php echo $row['id']; ?>" class="text"><?php echo $row['pertanyaan']; ?></span> 
              <input type="text" value="<?php echo $row['pertanyaan']; ?>" class="editbox" id="pertanyaan_input_<?php echo $row['id']; ?>"/>
            </td>

            <td class="edit_td">
               <span id="A_<?php echo $row['id']; ?>" class="text"><?php echo $row['A']; ?></span> 
              <input type="text" value="<?php echo $row['A']; ?>" class="editbox" id="A_input_<?php echo $row['id']; ?>"/>
            </td>
            
            <td class="edit_td">
               <span id="B_<?php echo $row['id']; ?>" class="text"><?php echo $row['B']; ?></span> 
              <input type="text" value="<?php echo $row['B']; ?>" class="editbox" id="B_input_<?php echo $row['id']; ?>"/>
            </td>
            
            <td class="edit_td">
               <span id="C_<?php echo $row['id']; ?>" class="text"><?php echo $row['C']; ?></span> 
              <input type="text" value="<?php echo $row['C']; ?>" class="editbox" id="C_input_<?php echo $row['id']; ?>"/>
            </td>

            <td class="edit_td">
               <span id="D_<?php echo $row['id']; ?>" class="text"><?php echo $row['D']; ?></span> 
              <input type="text" value="<?php echo $row['D']; ?>" class="editbox" id="D_input_<?php echo $row['id']; ?>"/>
            </td>
          </tr>
        </tbody>
      </table> 


<?php echo $this->load->view('js'); ?>
</html>
</body>