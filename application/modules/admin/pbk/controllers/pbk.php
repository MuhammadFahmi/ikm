<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pbk extends Admincore
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('pbk_model','model');
    }
    
    /* METHOD "READ"
     * berfungsi untuk membaca query dari database dengan system pagination
     */
    function index()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
    
        
     /* METHOD "SEARCH"*/
    function search()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['username'] = $this->session->userdata('username');
        $data['include'] =   $this->load->view('/search/include','',TRUE);
        $data['content'] =   $this->load->view('/search/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
    
    function create()
    {
	    $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters('<p class="text-error">', '</p> ');
	    $this->form_validation->set_rules('nama','Nama','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('single2','Single2','required|xss_clean|htmlspecialchars|trim');

        if ($this->form_validation->run() == FALSE)
        {
            $data['level'] = $this->session->userdata('level_id');
            $data['email'] = $this->session->userdata('email'); 
            $data['sub_unit'] = core::getAll("sub_unit","gammu"); 
            $data['content'] = $this->load->view('/create/content',$data,TRUE);
            $this->load->view('/admin/main',$data);
        }
        else
        {
            core::insert('pbk','gammu',array(
                    'GroupID' => $_REQUEST['single2'],
                    'Name'  => $_REQUEST['nama'],
                    'Number' => $_REQUEST['phone'],
                ));
            $this->session->set_flashdata('success','success');
            redirect('pbk/create');
        }
    }

    
    /* METHOD "UPDATE"
     * berfungsi untuk mengupdate data dari database
     */
    function update($id = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters(' <ul class="help-block"><li class="text-error">', '</li></ul> ');  
        
        $this->form_validation->set_rules('nama','Nama','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('single2','Single2','required|xss_clean|htmlspecialchars|trim');
        
        if ($this->form_validation->run('update') == FALSE)
        {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['sub_unit'] = core::getAll("sub_unit","gammu"); 
        $data['include'] =   $this->load->view('/update/include','',TRUE);
        $data['content'] =   $this->load->view('/update/content',$data,TRUE);
        $this->load->view("admin/main",$data);
        }
        else
        {
            core::update('pbk','gammu',array(
                'GroupID' => $this->input->post('single2'),
                'Name' => $this->input->post('nama'),
                'Number' => $this->input->post('phone'),
                ),$this->input->post('id'));
            $this->session->set_flashdata('success','success');
            redirect('pbk/update/'.$id);
        }
    }
    
    function detail($id = '')
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['username'] = $this->session->userdata('username');
        $data['include'] =   $this->load->view('/detail/include','',TRUE);
        $data['content'] =   $this->load->view('/detail/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
    // ACTIONS METHOD
    // method-method yang berfungsi hanya untuk sebuah actions/tidak ada view
    
    /* METHOD "DELETE"
     * berfungsi untuk menghapus data dari database
     */
    function delete($id = '')
    {
        core::delete('pbk','gammu','ID',$id);
        redirect('pbk');
    }
    

    function import()
    {
        if(isset($_REQUEST['upload'])){
            $data = array();
            if(!empty($_POST['hidden_input'])){
                $conf['upload_path'] = './upload/phonebook';
                $conf['allowed_types'] = 'xls';
                
                $this->load->library('upload',$conf);
                
                if(!$this->upload->do_upload('userfile')){
                
                    echo $this->upload->display_errors();
                    var_dump($this->upload->data());
                    exit(0);
                    
                }else{
                    
                    include_once ( APPPATH."libraries/excel_reader2.php");
                    $xl_data = new Spreadsheet_Excel_Reader($_FILES['userfile']['tmp_name']);
                    $j = 0;
                    $new_rows = 0;
                    $updated_rows = 0;
                    
                    //baris data dimulai dari baris ke 2
                    // echo $xl_data->rowcount($sheet_index=0);
                    //exit(0);
                    for ($i = 2; $i < ($xl_data->rowcount($sheet_index=0)); $i++){
                        
                    $id_subunit     = $xl_data->val($i, 1);
                    $nama_responden = $xl_data->val($i, 2);
                    $telepon        = $xl_data->val($i, 3);
                    
                        
                if(empty($nama_responden)) continue;
                
                 //key : email
                if(!core::dataExist('pbk','Number',$telepon)){
                    // echo "insert";
                    core::insert('pbk','gammu',array(
                             'GroupID'=>  $id_subunit,
                             'Name'  =>  $nama_responden,
                             'Number' =>  $telepon,
                        ));
                    $new_rows++;
                }else{
                    // echo "update";
                     //update
                    core::update_where("pbk","gammu",array(
                             'GroupID'=>  $id_subunit,
                             'Name'  =>  $nama_responden,
                             'Number' =>  $telepon,                    
                    ),'Number', $telepon);

                    $updated_rows++;
                }
                        
            }
                    
                    echo unlink('./upload/'. $_FILES['userfile']['name']);
                    $this->session->set_flashdata("k", "<div class=\"alert alert-success\" id=\"alert\">Data has been updated</div>");            
                    $this->session->set_flashdata('k',"<div class=\"alert alert-success\" id=\"alert\">Data berhasil diubah dengan ". $new_rows . " Data baru dan " . $updated_rows . " Data Update</div>");            
                    redirect('pbk');
                }
            
        } else {
            echo "string";
            }
        }else{

        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/import/content',$data,TRUE);
        $this->load->view("admin/main",$data);
        }
    } 
}