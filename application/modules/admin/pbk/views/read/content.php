<?php error_reporting(0);?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <?php echo $this->load->view('css_top')?>
</head>
<body>
    <!-- loaders -->
      <div class="loaders"></div>
    <!-- /loaders -->

    <div class="x_title">
      <h2 style="margin-top: 10px">Phonebook <small>Users</small></h2>
      <a href="<?php echo base_url('pbk/import');?>"><button type="submit" name="tambah" class="btn btn-primary" style="float: right;"><i class="fa fa-upload"></i> Import</button></a>   
      <a href="<?php echo base_url('pbk/create');?>"><button type="submit" name="tambah" class="btn btn-primary" style="float: right;"><i class="fa fa-plus-square"></i> Tambah</button></a>   
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- <p class="text-muted font-13 m-b-30">
        Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
      </p> -->
       <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
          <tr class="headings">
            <th>
              <input type="checkbox" id="check-all" class="flat">
            </th>
            <th>No</th>
            <th>Nama</th>
            <th>Nomor</th>
            <th>Sub unit</th>
            <th>Operasi</th>
          </tr>
        </thead>
        
        <tbody style="color: ">
          <?php 
            $groups = core::getAll('pbk','gammu');
              $no = 1;
              foreach($groups->result() as $row){
                $join = core::getJoin("pbk p","sub_unit s","s.id = p.GroupID","gammu",array('s.id' => $row->GroupID ));
              foreach ($join->result() as $value) {
                $i = $value->sub_unit;
                // print_r($value);
              }
            ?>
            <tr>
              <td class="a-center ">
                <input type="checkbox" class="flat" name="table_records">
              </td>
              <td><?php echo $no++?></td>
              <td><?php echo $row->Name?></td>
              <td><?php echo $row->Number?></td>
              <td><?php echo $i;?></td>
              <td>

                <a href="<?php echo base_url();?>pbk/delete/<?php echo $row->ID?>"><button class="btn btn-danger" onclick="javascript: return confirm('yakin hapus ??')" title="hapus phonebook"><i class="fa fa-trash"></i></button></a>

                <a href="<?php echo base_url();?>pbk/update/<?php echo $row->ID?>"><button class="btn btn-warning" title="update phonebook"><i class="fa fa-edit"></i></button></a>


                </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>

<?php echo $this->load->view('js'); ?>
</html>
</body>