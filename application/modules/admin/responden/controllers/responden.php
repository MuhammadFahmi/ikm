<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Responden extends Admincore
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('responden_model','model');
    }
    
    /* METHOD "READ"
     * berfungsi untuk membaca query dari database dengan system pagination
     */
    function index()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['responden'] = core::getAll("responden","gammu");
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }

    function listResponden()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $query = core::getWhere("sub_unit","gammu",array('email'=>$data['email']));
        foreach ($query->result() as $key) {
            $id_subunit = $key->id;
        }
        $data['responden'] = core::getWhere("responden","gammu", array('id_subunit'=> $id_subunit));
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/listResponden',$data,TRUE);
        $this->load->view("admin/main",$data);
    }

    function home()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/home',$data,TRUE);
        $this->load->view("admin/main",$data);
    }


    function create()
    {
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters('<p class="text-alert">', '</p> ');
        $this->form_validation->set_rules('nama','Nama','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('tempat_lahir','Tempat_lahir','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('birthday','Birthday','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('usia','Usia','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('alamat','Alamat','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('jenis_kelamin','Jenis_kelamin','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('single1','Single1','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('single2','Single2','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('email','Email','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('password','Password','required|xss_clean|htmlspecialchars|trim');
        
        $this->form_validation->set_rules('phone','Phone','required|xss_clean|htmlspecialchars|trim');
        
        if ($this->form_validation->run() == FALSE)
        {
            $data['level'] = $this->session->userdata('level_id');
            $data['email'] = $this->session->userdata('email');
            $data['sub_unit'] = core::getAll('sub_unit','gammu'); 
            $data['include']  = $this->load->view('/create/include','',TRUE);
            $data['content']  = $this->load->view('/create/content',$data,TRUE);
            $this->load->view('/admin/main',$data);
        }else{
            $email = core::getWhere("responden","gammu",array(
                'email'=>$this->input->post('email'),
            ));
            if($email->num_rows() == 0){
                $pekerjaan = $this->input->post('hobbies');
                for($a=0;$a<count($pekerjaan);$a++)
                {
                    core::insert('responden','gammu',array(
                        'nama_responden'  => $this->input->post('nama'),
                        'tempat_lahir'    => $this->input->post('tempat_lahir'),
                        'tanggal_lahir'   => date("Y-m-d", strtotime($this->input->post('birthday'))),
                        'usia' => $this->input->post('usia'),
                        'alamat' => $this->input->post('alamat'),
                        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                        'pendidikan_terakhir' => $this->input->post('single1'),
                        'pekerjaan' => $pekerjaan[$a],
                        'id_subunit' => $this->input->post("single2"),
                        'email' => $this->input->post("email"),
                        'telepon' => $this->input->post("phone"),
                    ));
                }
                    // variable untuk menampung data Pengguna
                    core::insert('admin','gammu',array(
                        'email'    => $this->input->post('email'),
                        'password' => sha1(md5($this->input->post('password'))),
                        'created'  => date("Y-m-d h:i:s"),
                        'level_id' => 3,
                    ));

                    $this->session->set_flashdata('success','success');
                    redirect('responden/create'); 
            }else{
                    $this->session->set_flashdata('error','error');
                    redirect('responden/create'); 

            }
        }    
    }

    /* METHOD "UPDATE"
     * berfungsi untuk mengupdate data dari database
     */
    function update($id = '')
    {
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters('<p class="text-alert">', '</p> ');
        $this->form_validation->set_rules('nama','Nama','required|xss_clean|htmlspecialchars|trim');
        $this->form_validation->set_rules('tempat_lahir','Tempat_lahir','required|xss_clean|htmlspecialchars|trim');
        
        if ($this->form_validation->run() == FALSE)
        {
            $data['level'] = $this->session->userdata('level_id');
            $data['email'] = $this->session->userdata('email');
            $data['sub_unit'] = core::getAll('sub_unit','gammu'); 
            $data['include']  = $this->load->view('/update/include','',TRUE);
            $data['content']  = $this->load->view('/update/content',$data,TRUE);
            $this->load->view('/admin/main',$data);
        }else{
            // echo "string";
            $pekerjaan = $this->input->post('hobbies');
            for($a=0;$a<count($pekerjaan);$a++){
                core::update_where('responden','gammu',array(
                    'nama_responden'  => $this->input->post('nama'),
                    'tempat_lahir'    => $this->input->post('tempat_lahir'),
                    'tanggal_lahir'   => date("Y-m-d", strtotime($this->input->post('birthday'))),
                    'usia' => $this->input->post('usia'),
                    'alamat' => $this->input->post('alamat'),
                    'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                    'pendidikan_terakhir' => $this->input->post('single1'),
                    'pekerjaan' => $pekerjaan[$a],
                    'id_subunit' => $this->input->post("single2"),
                    'email' => $this->input->post("email"),
                    'telepon' => $this->input->post("phone"),
            ), 'email',$this->input->post("email"));

            // // variable untuk menampung data Pengguna
                $id_r = $id+1;
                core::update_where('admin','gammu',array(
                    'email' => $this->input->post("email"),
                    'password' => sha1(md5($this->input->post("password"))),
                    'created' => date("Y-m-d h:i:s"),
                    'level_id' => 3,
                ), 'email',$this->input->post("email"));
            }    
            $this->session->set_flashdata('success','success');
            redirect('responden/update/'.$id); 
        }
    }
    
    // ACTIONS METHOD
    // method-method yang berfungsi hanya untuk sebuah actions/tidak ada view
    
    /* METHOD "DELETE"
     * berfungsi untuk menghapus data dari database
     */
    function delete($id = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters('<p class="text-alert">', '</p> ');
        $data = core::getWhere('responden','gammu',array(
            'id'=>$id,
        ));
        foreach($data->result() as $key){
            $email = $key->email;
        }
        
        core::delete('admin','gammu','email',$email);
        core::delete('responden','gammu','id',$id);
     
        $this->session->set_flashdata('success','success');
        redirect('responden');
    }

    function import()
    {
        if(isset($_REQUEST['upload'])){
            $data = array();
            if(!empty($_POST['hidden_input'])){
                $conf['upload_path'] = './upload';
                $conf['allowed_types'] = 'xls';
                
                $this->load->library('upload',$conf);
                
                if(!$this->upload->do_upload('userfile')){
                
                    echo $this->upload->display_errors();
                    var_dump($this->upload->data());
                    exit(0);
                    
                }else{
                    
                    include_once ( APPPATH."libraries/excel_reader2.php");
                    $xl_data = new Spreadsheet_Excel_Reader($_FILES['userfile']['tmp_name']);
                    $j = 0;
                    $new_rows = 0;
                    $updated_rows = 0;
                    
                    //baris data dimulai dari baris ke 2
                    // echo $xl_data->rowcount($sheet_index=0);
                    //exit(0);
                    for ($i = 2; $i < ($xl_data->rowcount($sheet_index=0)); $i++){
                        
                    $nama_responden = $xl_data->val($i, 1);
                    $tempat_lahir   = $xl_data->val($i, 2);
                    $tanggal_lahir  = $xl_data->val($i, 3);
                    $tgl_lahir      = date("Y-m-d", strtotime($tanggal_lahir));
                    $usia           = $xl_data->val($i, 4);
                    $alamat         = $xl_data->val($i, 5);
                    $jenis_kelamin  = $xl_data->val($i, 6);
                    $pendidikan_terakhir = $xl_data->val($i, 7);
                    $pekerjaan      = $xl_data->val($i, 8);
                    $id_subunit     = $xl_data->val($i, 9);
                    $email          = $xl_data->val($i, 10);
                    $password       = $xl_data->val($i, 11);
                    $passwords      = sha1(md5($password));
                    $telepon        = $xl_data->val($i, 12);
                    
                        
                if(empty($nama_responden)) continue;
                
                 //key : email
                if(!core::dataExist('responden','email',$email)){
                    // echo "insert";
                    core::insert('responden','gammu',array(
                             'nama_responden'=>  $nama_responden,
                             'tempat_lahir'  =>  $tempat_lahir,
                             'tanggal_lahir' =>  $tgl_lahir,
                             'usia'          =>  $usia,
                             'alamat'        =>  $alamat,
                             'jenis_kelamin' =>  $jenis_kelamin,
                             'pendidikan_terakhir'=>  $pendidikan_terakhir,
                             'pekerjaan'     =>  $pekerjaan,
                             'id_subunit'    =>  $id_subunit,
                             'email'         =>  $email,
                             'telepon'       =>  $telepon,
                        ));
                        
                    core::insert('admin','gammu',array(
                        'email' =>$email,
                        'password'=>$passwords,
                        'created'=>date("Y-m-d h:i:s"),
                        'level_id'=>3
                    ));
                    $new_rows++;
                }else{
                    // echo "update";
                     //update
                    core::update_where("responden","gammu",array(
                         'nama_responden'=>  $nama_responden,
                         'tempat_lahir'  =>  $tempat_lahir,
                         'tanggal_lahir' =>  $tgl_lahir,
                         'usia'          =>  $usia,
                         'alamat'        =>  $alamat,
                         'jenis_kelamin' =>  $jenis_kelamin,
                         'pendidikan_terakhir'=>  $pendidikan_terakhir,
                         'pekerjaan'     =>  $pekerjaan,
                         'id_subunit'    =>  $id_subunit,
                         'email'         =>  $email,
                         'telepon'       =>  $telepon,
                    ),'email', $email);

                    core::update_where('admin','gammu',array(
                        'email' =>$email,
                        'password'=>$passwords,
                        'created'=>date("Y-m-d h:i:s"),
                        'level_id'=>3
                    ),'email', $email);

                    $updated_rows++;
                }
                        
            }
                    
                    echo unlink('./upload/'. $_FILES['userfile']['name']);
                    //$this->session->set_flashdata("k", "<div class=\"alert alert-success\" id=\"alert\">Data has been updated</div>");            
                    $this->session->set_flashdata('k',"<div class=\"alert alert-success\" id=\"alert\">Data berhasil diubah dengan ". $new_rows . " Data baru dan " . $updated_rows . " Data Update</div>");            
                    redirect('responden');
                }
            
        } else {
            echo "string";
            }
        }else{

        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/import/content',$data,TRUE);
        $this->load->view("admin/main",$data);
        }
    }
}