<legend>Kirim Pesan</legend>
  
<?php include('layout.php');?>  
<?php error_reporting(0); ?>

    <div class="x_content">
         
    
   <form method='POST' action='<?php echo base_url('send_message/send_by_group');?>' charset='UTF-8' class="form-horizontal form-label-left" novalidate>

    
         <?php $query = core::get_all('sub_unit','gammu');?>
          <div class="form-group">
            <label>Kontak<span class="required">*</span></label>
            <!-- <div class="col-md-9 col-sm-9 col-xs-12"> -->
              <select class="select2_single form-control" tabindex="-1" style="width: 680px;" id="single2" name="group">
                <option value="empty">Sub Unit</option>
                <?php foreach ($query->result_array() as $key) { ?>                           
                <option value="<?php echo $key['id']?>">  <?php echo $key['sub_unit'] ?>
                </option>
                <?php } ?>
              </select>
            <!-- </div> -->
          </div>
     
          <div class="item form-group">
            <label>Pesan <span class="required">*</span>
            </label>
            <!-- <div class="col-md-6 col-sm-6 col-xs-12"> -->
              <textarea id='message' class='span9' style='height:200px;width: 680px' name='message' placeholder='message'><?php echo set_value('message');?></textarea>
            <!-- </div> -->
          </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <!-- <div class="col-md-6 col-md-offset-3"> -->
              <button id="send" type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Kirim Pesan</button>
              <button type="reset" class="btn btn-danger"><i class="fa fa-remove"></i> Batal</button>
            <!-- </div> -->
          </div>
  </form>
  
  <?php echo $this->load->view('js'); ?>

      
 
    <script src="<?php echo base_url();?>assets/plugin/count-textarea/count-textarea.js"></script>              
 <script>
                          var options = {
				'maxCharacterSize': 160,
				'originalStyle': 'originalDisplayInfo',
				'warningStyle': 'warningDisplayInfo',
				'warningNumber': 40,
				'displayFormat': '#input Characters | #left Characters Left | #words Words'
			};
			$('#message').textareaCount(options);
		
 </script>