<?php error_reporting(0) ?>
    <!-- loaders -->
      <!-- <div class="loaders"></div> -->
    <!-- /loaders -->                    
    <div class="x_title">
      <h2 style="margin-top: 10px" ><i class='fa fa-envelope-o'></i> Sending <small>Users</small></h2>

      <div class="clearfix"></div>
    </div>
    
    <div class="x_content">
       <!-- <p class="text-muted font-13 m-b-30">
        Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
      </p> -->
       <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
          <tr class="headings">
            <th>No</th>
            <th>Kepada</th>
            <!-- <th>Pesan</th> -->
            <th>Tanggal</th>
            <th>Status</th>
            <th>Aksi</th>
          </tr>
        </thead>
       
        <tbody style="color: ">
        <?php $outbox = core::getAll('sentitems','gammu');?>
          <?php $no = 1; foreach ($outbox->result() as $key ) { ?>
            <tr>
              <td><?php echo $no++?></td>
              <td><?php echo $key->DestinationNumber?></td>
              <!-- <td><?php //echo character_limiter($key->TextDecoded,25) ;?></td> -->
              <td><?php echo $key->SendingDateTime?></td>
              <td><?php echo $key->Status == "SendingOKNoReport" ? "Success":"Failed" ;?></td>
              <td>
                <a href="<?php echo base_url();?>sending/delete/<?php echo $key->ID?>"><button class="btn btn-danger" onclick="javascript: return confirm('yakin hapus ??')"><i class="fa fa-trash"></i></button></a>

                <a href="<?php echo base_url();?>sending/detail/<?php echo $key->ID?>"><button class="btn btn-danger"><i class="fa fa-weibo"></i></button></a>


                </td>
            </tr>
          <?php 
            
          }
           ?>
        </tbody>
      </table>
