<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sub_unit extends Admincore
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('responden_model','model');
    }
    
    /* METHOD "READ"
     * berfungsi untuk membaca query dari database dengan system pagination
     */
    function index()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['sub_unit'] = core::getAll("sub_unit","gammu");
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }

    function home()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/home',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
    
    function create()
    {
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters('<p class="text-alert">', '</p> ');
        $this->form_validation->set_rules('nama','Nama','required|xss_clean|htmlspecialchars|trim');
        $this->form_validation->set_rules('alamat','Alamat','required|xss_clean|htmlspecialchars|trim');
        $this->form_validation->set_rules('phone','Phone','required|xss_clean|htmlspecialchars|trim');
        $this->form_validation->set_rules('koordinator','Koordinator','required|xss_clean|htmlspecialchars|trim');

        if ($this->form_validation->run() == FALSE)
        {
            $data['level'] = $this->session->userdata('level_id');
            $data['email'] = $this->session->userdata('email');
            $data['sub_unit'] = core::getAll('sub_unit','gammu'); 
            $data['include']  = $this->load->view('/create/include','',TRUE);
            $data['content']  = $this->load->view('/create/content',$data,TRUE);
            $this->load->view('/admin/main',$data);
        }else{
            core::insert('sub_unit','gammu',array(
                'sub_unit'  => $this->input->post('nama'),
                'alamat' => $this->input->post('alamat'),
                'telepon' => $this->input->post('phone'),
                'koordinator' => $this->input->post('koordinator'),
                'mesin_peralatan' => $this->input->post('mesin_peralatan'),
                'potensi_ikm' => $this->input->post('potensi_ikm'),
                'email' => $this->input->post('email'),
            ));
            core::insert('admin','gammu',array(
                'email'  => $this->input->post('email'),
                'password' => sha1(md5(111)),
                'created' => date("Y-m-d h:i:s"),
                'level_id' => 2,
            ));
            
            $this->session->set_flashdata('success','success');
            redirect('sub_unit/create');
        }
    }

    /* METHOD "UPDATE"
     * berfungsi untuk mengupdate data dari database
     */
    function update($id = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters(' <ul class="help-block"><li class="text-error">', '</li></ul> ');  
        
        $this->form_validation->set_rules('nama','Nama','required|xss_clean|htmlspecialchars|trim');
        $this->form_validation->set_rules('alamat','Alamat','required|xss_clean|htmlspecialchars|trim');
        $this->form_validation->set_rules('phone','Phone','required|xss_clean|htmlspecialchars|trim');
        $this->form_validation->set_rules('koordinator','Koordinator','required|xss_clean|htmlspecialchars|trim');
        
        if ($this->form_validation->run() == FALSE)
        {
            $data['level'] = $this->session->userdata('level_id');
            $data['email'] = $this->session->userdata('email');
            $data['sub_unit'] = core::getAll('sub_unit','gammu'); 
            $data['include']  = $this->load->view('/update/include','',TRUE);
            $data['content']  = $this->load->view('/update/content',$data,TRUE);
            $this->load->view('/admin/main',$data);
        }else{
            core::update('sub_unit','gammu',array(
                'sub_unit'  => $this->input->post('nama'),
                'alamat' => $this->input->post('alamat'),
                'telepon' => $this->input->post('phone'),
                'koordinator' => $this->input->post('koordinator'),
                'mesin_peralatan' => $this->input->post('mesin_peralatan'),
                'potensi_ikm' => $this->input->post('potensi_ikm'),
                'email' => $this->input->post('email'),
            ), $id);
            
            core::update_where('admin','gammu',array(
                'email'  => $this->input->post('email'),
                'password' => sha1(md5(111)),
                'created' => date("Y-m-d h:i:s"),
                'level_id' => 2,
            ),'email',$email);
            
            $this->session->set_flashdata('success','success');
            redirect('sub_unit/update/'.$id);
        }
    }
    
     // ACTIONS METHOD
    // method-method yang berfungsi hanya untuk sebuah actions/tidak ada view
    
    /* METHOD "DELETE"
     * berfungsi untuk menghapus data dari database
     */
    function delete($id = '')
    {        
         $data = core::getWhere('sub_unit','gammu',array(
            'id'=>$id,
        ));
        foreach($data->result() as $key){
            $email = $key->email;
        }
        
        core::delete('admin','gammu','email',$email);
        core::delete('sub_unit','gammu','id',$id);
        $this->session->set_flashdata('success','success');
        redirect('sub_unit');
    }

    function import()
    {
        if(isset($_REQUEST['upload'])){
            $data = array();
            if(!empty($_POST['hidden_input'])){
                $conf['upload_path'] = './upload/sub_unit';
                $conf['allowed_types'] = 'xls';
                
                $this->load->library('upload',$conf);
                
                if(!$this->upload->do_upload('userfile')){
                
                    echo $this->upload->display_errors();
                    var_dump($this->upload->data());
                    exit(0);
                    
                }else{
                    
                    include_once ( APPPATH."libraries/excel_reader2.php");
                    $xl_data = new Spreadsheet_Excel_Reader($_FILES['userfile']['tmp_name']);
                    $j = 0;
                    $new_rows = 0;
                    $updated_rows = 0;
                    
                    //baris data dimulai dari baris ke 2
                    // echo $xl_data->rowcount($sheet_index=0);
                    //exit(0);
                    for ($i = 2; $i < ($xl_data->rowcount($sheet_index=0)); $i++){
                        
                    $sub_unit       = $xl_data->val($i, 1);
                    $alamat         = $xl_data->val($i, 2);
                    $telepon        = $xl_data->val($i, 3);
                    $mesin_peralatan= $xl_data->val($i, 4);
                    $potensi_ikm    = $xl_data->val($i, 5);
                    $koordinator    = $xl_data->val($i, 6);
                    $email          = $xl_data->val($i, 7);
                    
                if(empty($sub_unit)) continue;
                
                 //key : email
                if(!core::dataExist('sub_unit','email',$email)){
                    // echo "insert";
                    core::insert('sub_unit','gammu',array(
                             'sub_unit'=>  $sub_unit,
                             'alamat'  =>  $alamat,
                             'telepon' =>  $telepon,
                             'koordinator' =>  $koordinator,
                             'mesin_peralatan'=>  $mesin_peralatan,
                             'potensi_ikm' =>  $potensi_ikm,
                             'email'   =>  $email,
                        ));
                        
                    core::insert('admin','gammu',array(
                        'email' =>$email,
                        'password'=>sha1(md5(111)),
                        'created'=>date("Y-m-d h:i:s"),
                        'level_id'=>2
                    ));
                    $new_rows++;
                }else{
                    // echo "update";
                     //update
                    core::update_where("sub_unit","gammu",array(
                         'sub_unit'=>  $sub_unit,
                             'alamat'  =>  $alamat,
                             'telepon' =>  $telepon,
                             'koordinator' =>  $koordinator,
                             'mesin_peralatan'=>  $mesin_peralatan,
                             'potensi_ikm' =>  $potensi_ikm,
                             'email'   =>  $email,
                    ),'email', $email);

                    core::update_where('admin','gammu',array(
                        'email' =>$email,
                        'password'=>sha1(md5(111)),
                        'created'=>date("Y-m-d h:i:s"),
                        'level_id'=>2
                    ),'email', $email);

                    $updated_rows++;
                }
                        
            }
                    
                    echo unlink('./upload/'. $_FILES['userfile']['name']);
                    //$this->session->set_flashdata("k", "<div class=\"alert alert-success\" id=\"alert\">Data has been updated</div>");            
                    $this->session->set_flashdata('k',"<div class=\"alert alert-success\" id=\"alert\">Data berhasil diubah dengan ". $new_rows . " Data baru dan " . $updated_rows . " Data Update</div>");            
                    redirect('sub_unit');
                }
            
        } else {
            echo "string";
            }
        }else{

        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/import/content',$data,TRUE);
        $this->load->view("admin/main",$data);
        }
    }
}