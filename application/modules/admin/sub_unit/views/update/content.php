<?php error_reporting(0);?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <?php echo $this->load->view('css_top')?>
</head>
<body>
    <!-- loaders -->
      <div class="loaders"></div>
    <!-- /loaders -->
                
      <div class="x_title">
        <h2>Form Update Sub Unit <small>sub title</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>

        <div class="clearfix"></div>
      </div>      
      <div class="x_content">
        <?php 
            $query = core::get_wheres('sub_unit','gammu',array('id' => $this->uri->segment(3)),1); 
            $row = $query->row_array();
        ?>
        <?php if($this->session->flashdata('success')){?>
        <div class="alert alert-info alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Success:</span>
            Update Data Sub Unit Berhasil
        </div>
        <?php }?>
        <form method="post" action="<?php echo site_url('sub_unit/update/'.$row['id']) ?>" charset='UTF-8' class="form-horizontal form-label-left" novalidate>

          <input type="hidden" class="id_r" value=<?php echo $row['id'];?> >


          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Sub Unit <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="nama" class="form-control col-md-7 col-xs-12" name="nama" placeholder="Enter......" required="required" type="text" value="<?php echo $row['sub_unit']?>">
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Alamat <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea id="alamat" required="required" name="alamat" class="form-control col-md-7 col-xs-12"><?php echo $row['alamat']?></textarea>
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Telepon <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="tel" id="telephone" name="phone" required="required" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12" placeholder="Enter......" value="<?php echo $row['telepon']?>">
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Koordinator <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="koordinator" class="form-control col-md-7 col-xs-12" name="koordinator" placeholder="Enter......" required="required" type="text" value="<?php echo $row['koordinator']?>">
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Jumlah Mesin <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="mesin_peralatan" class="form-control col-md-7 col-xs-12" name="mesin_peralatan" placeholder="Enter......" required="required" type="text" value="<?php echo $row['mesin_peralatan']?>">
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Potensi IKM <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="potensi_ikm" class="form-control col-md-7 col-xs-12" name="potensi_ikm" placeholder="Enter......" required="required" type="text" value="<?php echo $row['potensi_ikm']?>">
            </div>
          </div>

          <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Email <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="email" class="form-control col-md-7 col-xs-12" name="email" placeholder="Enter......" required="required" type="text" value="<?php echo $row['email']?>">
          </div>
        </div>


        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-md-offset-3">
            <button id="send" type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Update</button>
            <button type="submit" class="btn btn-danger"><i class="fa fa-remove"></i> Batal</button>
          </div>
        </div>
        </form>

<?php echo $this->load->view('js'); ?>
</html>
</body>