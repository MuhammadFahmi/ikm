<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Unsur extends Admincore
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('unsur_model','model');
    }
    
    /* METHOD "READ"
     * berfungsi untuk membaca query dari database dengan system pagination
     */
    function index()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['email'] = $this->session->userdata('email');
        $data['unsur'] = core::getAll("unsur","gammu");
        $data['include'] =   $this->load->view('/read/include','',TRUE);
        $data['content'] =   $this->load->view('/read/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
    
        
     /* METHOD "SEARCH"*/
    function search()
    {
        $data['level'] = $this->session->userdata('level_id');
        $data['username'] = $this->session->userdata('username');
        $data['include'] =   $this->load->view('/search/include','',TRUE);
        $data['content'] =   $this->load->view('/search/content',$data,TRUE);
        $this->load->view("admin/main",$data);
    }
    
    function create()
    {
        $this->load->library('session');
        // $this->form_validation->CI =& $this;
        // $this->form_validation->set_error_delimiters('<p class="text-error">', '</p> ');
       if(isset($_REQUEST['jsonObj']))
       {
            // echo "ahahah";
            $data = $this->input->post('jsonObj');
            // print_r($data);
            foreach ($data as $key ) {
            // print_r($key);
            core::insert('unsur','gammu',array(
                'id_unsur'  => $key['id_unsur'],
                'nama_unsur' => $key['nama_unsur'],
            ));
            }
       }
       else{
            $data['level'] = $this->session->userdata('level_id');
            $data['email'] = $this->session->userdata('email'); 
            $data['id_unsur'] = core::generateID('unsur','gammu','id_unsur','2');
            $data['content']  = $this->load->view('/create/content',$data,TRUE);
            $this->load->view('/admin/main',$data);
       }
    }

    /* METHOD "UPDATE"
     * berfungsi untuk mengupdate data dari database
     */
    function update($id = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters(' <ul class="help-block"><li class="text-error">', '</li></ul> ');  
        
        // $this->form_validation->set_rules('nama','Nama','required|xss_clean|htmlspecialchars|trim');
        // $this->form_validation->set_rules('tempat_lahir','Tempat_lahir','required|xss_clean|htmlspecialchars|trim');

        if (!isset($_REQUEST['send']))
        {
            // echo "salah";
            $data['level'] = $this->session->userdata('level_id');
            $data['email'] = $this->session->userdata('email');
            $data['include'] =   $this->load->view('/update/include','',TRUE);
            $data['content'] =   $this->load->view('/update/content',$data,TRUE);
            $this->load->view("admin/main",$data);
        }else{

            core::update_where('unsur','gammu',array(
                'id_unsur'  => $this->input->post('id_unsur'),
                'nama_unsur'=> $this->input->post('nama'),
            ), 'id_unsur', $id);
            $this->session->set_flashdata('success','success');
            redirect('unsur/update/'.$id); 
       }
    }
    // ACTIONS METHOD
    // method-method yang berfungsi hanya untuk sebuah actions/tidak ada view
    
    /* METHOD "DELETE"
     * berfungsi untuk menghapus data dari database
     */
    function delete($id = '')
    {        
        core::delete('unsur','gammu','id_pertanyaan',$id);
        $this->session->set_flashdata('success','success');
        redirect('unsur');
    }
}