<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Unsur_model extends CI_model
{
     function __construct()
    {
        parent::__construct();
    }

    function generateID()
    {
    	// $db = $this->load->database('gammu',TRUE);
    	$q = $this->db->query("select MAX(RIGHT(id_unsur,3)) as kd_max from unsur");
		$kd = "";
		if($q->num_rows()>0)
		{
			foreach($q->result() as $k)
			{
				$tmp = ((int)$k->kd_max)+1;
				$kd = sprintf("%03s", $tmp);
			}
		}
		else
		{
			$kd = "001";
		}	
		return $kd;

    }

     // BUAT METHOD MODEL DI SINI
}