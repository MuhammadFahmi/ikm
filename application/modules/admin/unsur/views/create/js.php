
  <!-- form validation -->
  <script src="<?php echo base_url();?>assets/gantella/js/validator/validator.js"></script>
  
     
     <!-- parsely -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/gantella/js/parsley/parsley.min.js"></script>
                  
  
  <!-- daterangepicker -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/gantella/js/moment/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/gantella/js/datepicker/daterangepicker.js"></script>
    
  <script type="text/javascript">
  
   // <!-- form validation -->
    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#demo-form .btn').on('click', function() {
        $('#demo-form').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#demo-form').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });

    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#demo-form2 .btn').on('click', function() {
        $('#demo-form2').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#demo-form2').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });
    try {
      hljs.initHighlightingOnLoad();
    } catch (err) {}
  </script>

  <script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      // .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // var jsonObj = $(this).serialize();
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
        // alert("gagal");
      }
      else{
        // this.submit();
        add_item();
        // if(submit)
      return false;
    }     
  
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);
  </script>  

  <script type="text/javascript">
        
        $(document).ready(function(){
          $(document).on('click', '#hapus', function(e) {
            var index = $('#hapus').index(this);
            var id_unsur    = $('#id_unsur');         
            var nama_unsur  = $('#nama');         
            var panjangData = id_unsur.val().length;
            var kode_string = id_unsur.val().substr("1","1");
            var kode_number = id_unsur.val().substr("2",panjangData);
          
            $('.baris').eq(index).remove();
            for(var a = 0; a < kode_number.length; a++ ){
              inc = parseInt(kode_number) - 1;
            }
            id_unsur.val("U"+(kode_string+inc));
          });
        });

        function add_item()
        {         
          var id_unsur    = $('#id_unsur');         
          var nama_unsur  = $('#nama');         
          var panjangData = id_unsur.val().length;
          var kode_string = id_unsur.val().substr("1","1");
          var kode_number = id_unsur.val().substr("2",panjangData);
          $(".details").show();  // alert(kode_string.length);
            // alert(panjangData);
            // alert(kode_string);
        
          var count = $('.pk[nilai="'+id_unsur.val()+'"]').length;

          if (count==0){
            $('#datatable-responsive tbody').append(
              "<tr class='baris'>" +
              "<td style='display:none' class='pk' nilai="+id_unsur.val() +" kd="+kode_number+" >" + id_unsur.val() + "</td>" +
              "<td class='id_unsur["+kode_number+"]'>" + "<input type='text' value="+ id_unsur.val() + " class='form-control'>" + "</td>" +
              "<td class='nama_unsur'>" + "<input type='text' value="+ nama_unsur.val() + " class='form-control' id='unsur'>" + "</td>" +
              "<td class='hapus'><center><button class='btn btn-danger' id='hapus'>X</button></center></td>" +
              "</tr>"
            );
              if(panjangData <= 3){
                for(var a = 0; a < kode_number.length; a++ )
                {
                  if(kode_number == 9){
                    kode_string = parseInt(kode_string) + 1 ;
                    id_unsur.val("U"+ kode_string +"0");
                    nama_unsur.val("").focus();  
                  }
                  else{
                    inc = parseInt(kode_number) + 1; 
                    id_unsur.val("U"+(kode_string+inc));
                    nama_unsur.val("").focus();
                  }
                }
              }
              else{
                
            }
          }
          else{
  
          }
        }

        function kirim(){
          var status = "kirim";
          var jml = $('.baris').length;
          var jsonObj = [];
          
          $(".baris").each(function() {
          var id_unsur   = $(this).find('.pk').html();
          var nama_unsur = $(this).find('#unsur').val();
          // alert(nama_unsur);
          item = {}
          item ["id_unsur"]   = id_unsur;
          item ["nama_unsur"] = nama_unsur;
          jsonObj.push(item);
          });
             
             $.ajax({
              type : "post",
              url  : "<?php echo base_url('unsur/create') ?>", 
              data : {jsonObj :jsonObj},
              cache : false,
              // dataType : "json",
              success: function(msg){
                  // alert(msg);
                  document.location = "<?php echo base_url('unsur')?>"; 
              },
              error:function(msg){
                alert("error js");
                // alert('Harus Diisi!!');
              }
            });
          }
        </script>