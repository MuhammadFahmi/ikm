<?php error_reporting(0)?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
    <!-- loaders -->
      <div class="loaders"></div>
    <!-- /loaders -->
    <div class="x_title">
      <h2 style="margin-top: 10px">Data Unsur Pelayanan <small>Users</small></h2>
      <a href="<?php echo base_url('unsur/create');?>"><button type="submit" name="tambah" class="btn btn-primary" style="float: right;"><i class="fa fa-plus-square"></i> Tambah</button></a>   
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php if($this->session->flashdata('success')){?>
        <div class="alert alert-info alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Success:</span>
            Delete Data Unsur Berhasil 
        </div>
      <?php }?>
    <!-- <p class="text-muted font-13 m-b-30">
        Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
      </p> -->
       <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead style="background-color: #1abb9c;color: white">
        <!-- #34495e
        #1abb9c -->
            <th>
              <input type="checkbox" id="check-all" class="flat">
            </th>
            <th>Id Unsur</th>
            <th>Unsur</th>
            <th>Operasi</th>
          </tr>
        </thead>
        
        <tbody style="color: ">
        <?php foreach ($unsur->result() as $key ) {?>
            <tr>
              <td class="a-center ">
                <input type="checkbox" class="flat" name="table_records">
              </td>
              <td><?php echo $key->id_unsur?></td>
              <td><?php echo $key->nama_unsur?></td>
              <td>

                <a href="<?php echo base_url();?>unsur/delete/<?php echo $key->id_pertanyaan?>"><button class="btn btn-danger" onclick="javascript: return confirm('yakin hapus ??')"><i class="fa fa-remove"></i></button></a>

                <a href="<?php echo base_url();?>unsur/update/<?php echo $key->id_unsur?>"><button class="btn btn-warning" title="update"><i class="fa fa-edit"></i></button></a>


                </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>

<?php //echo $this->load->view('js'); ?>
</html>
</body>