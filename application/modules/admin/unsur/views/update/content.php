<?php error_reporting(0);?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  
  <style type="text/css">
  .loaders {position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: url('../../assets/loaders/ajax-loader1.gif') 50% 50% no-repeat #f2f2f2;opacity: 0.9;filter: alpha(opacity=90);}
  #batal{margin-left: 280px;position: relative;top: -49px;}
      
  </style>

</head>
<body>
    <!-- loaders -->
      <div class="loaders"></div>
    <!-- /loaders -->
      <?php $query = core::get_where('unsur','gammu',array('id_unsur' => $this->uri->segment(3)),1); ?>
      <?php $row = $query->row_array();
      ?>

    <div class="x_title">
      <h2>Form Tambah Unsur IKM <small>sub title</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>

      <div class="clearfix"></div>
    </div>
    <div class="x_content">
          <?php if($this->session->flashdata('success')){?>
        <div class="alert alert-info alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Success:</span>
            Update Data Unsur Berhasil 
        </div>
      <?php }?>

    <form method="post" action="<?php echo site_url('unsur/update/'.$row['id_unsur']) ?>" charset='UTF-8' class="form-horizontal form-label-left" novalidate>
     <input type="hidden" value="<?php //$row['id']?>" class="idr"></input>
        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_unsur">Id Unsur <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="id_unsur" class="form-control col-md-7 col-xs-12" value="<?php echo $row['id_unsur']?>" name="id_unsur" placeholder="Enter......" required="required" type="text" readonly>
          </div>
        </div>

        <div class="item form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Unsur <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="nama" class="form-control col-md-7 col-xs-12" name="nama" placeholder="Enter......" required="required" type="text" value="<?php echo $row['nama_unsur'] ?>">
          </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-md-offset-3">
            <button id="send" name="send" type="submit" class="btn btn-success"><i class="fa fa-send"></i> Update</button>
          </div>
        </div>
      </form>
            <button type="submit" id="batal" class="btn btn-danger"><i class="fa fa-close"></i> Batal</button>

<?php echo $this->load->view('js'); ?>
</html>
</body>