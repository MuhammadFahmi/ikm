-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Agu 2016 pada 11.45
-- Versi Server: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gammu`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `level_id` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`email`, `password`, `created`, `level_id`) VALUES
('aci@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:36', 3),
('ade@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-04 03:33:27', 2),
('admin@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-02 16:27:26', 1),
('agus@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-04 03:33:27', 2),
('al@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-05 01:49:14', 3),
('alex@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-04 03:33:27', 2),
('ardi@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:36', 3),
('arya@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:35', 3),
('ayu@yahoo.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:36', 3),
('detty@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-04 03:37:01', 2),
('eka@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-04 03:33:27', 2),
('erik@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-04 03:33:27', 2),
('gina@yahoo.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:35', 3),
('ira@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:35', 3),
('joko@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-04 03:33:27', 2),
('kelvin@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:36', 3),
('lambok@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:35', 3),
('lesmana@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-04 03:33:27', 2),
('nining@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-04 03:33:27', 2),
('pudja@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:35', 3),
('rohmat@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:36', 3),
('ruhiyat@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-04 03:33:27', 2),
('swisma@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:35', 3),
('try@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:36', 3),
('wildan@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:35', 3),
('wildanegi011@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-05 01:18:15', 3),
('wildanegi@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:35', 3),
('yuli@gmail.com', '3be0ff98032936bc7f9df51c5685ee5f2dd6ccee', '2016-08-03 05:43:35', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ahp`
--

CREATE TABLE IF NOT EXISTS `ahp` (
  `id` int(11) NOT NULL,
  `kriteria1` varchar(10) NOT NULL,
  `kriteria2` varchar(10) NOT NULL,
  `skala` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ahp`
--

INSERT INTO `ahp` (`id`, `kriteria1`, `kriteria2`, `skala`) VALUES
(1, 'U01', 'U01', 1),
(2, 'U01', 'U02', 3),
(3, 'U01', 'U03', 5),
(4, 'U01', 'U04', 5),
(5, 'U01', 'U05', 5),
(6, 'U01', 'U06', 2),
(7, 'U01', 'U07', 0.2),
(8, 'U01', 'U08', 7),
(9, 'U01', 'U09', 0.2),
(10, 'U01', 'U10', 2),
(11, 'U01', 'U11', 0),
(12, 'U01', 'U12', 0),
(13, 'U01', 'U13', 0),
(14, 'U01', 'U14', 0),
(15, 'U02', 'U02', 1),
(16, 'U02', 'U03', 0),
(17, 'U02', 'U04', 0),
(18, 'U02', 'U05', 0),
(19, 'U02', 'U06', 0),
(20, 'U02', 'U07', 0),
(21, 'U02', 'U08', 0),
(22, 'U02', 'U09', 0),
(23, 'U02', 'U10', 0),
(24, 'U02', 'U11', 0),
(25, 'U02', 'U12', 0),
(26, 'U02', 'U13', 0),
(27, 'U02', 'U14', 0),
(28, 'U03', 'U03', 1),
(29, 'U03', 'U04', 0),
(30, 'U03', 'U05', 0),
(31, 'U03', 'U06', 0),
(32, 'U03', 'U07', 0),
(33, 'U03', 'U08', 0),
(34, 'U03', 'U09', 0),
(35, 'U03', 'U10', 0),
(36, 'U03', 'U11', 0),
(37, 'U03', 'U12', 0),
(38, 'U03', 'U13', 0),
(39, 'U03', 'U14', 0),
(40, 'U04', 'U04', 0),
(41, 'U04', 'U05', 0),
(42, 'U04', 'U06', 0),
(43, 'U04', 'U07', 0),
(44, 'U04', 'U08', 0),
(45, 'U04', 'U09', 0),
(46, 'U04', 'U10', 0),
(47, 'U04', 'U11', 0),
(48, 'U04', 'U12', 0),
(49, 'U04', 'U13', 0),
(50, 'U04', 'U14', 0),
(51, 'U05', 'U05', 1),
(52, 'U05', 'U06', 0),
(53, 'U05', 'U07', 0),
(54, 'U05', 'U08', 0),
(55, 'U05', 'U09', 0),
(56, 'U05', 'U10', 0),
(57, 'U05', 'U11', 0),
(58, 'U05', 'U12', 0),
(59, 'U05', 'U13', 0),
(60, 'U05', 'U14', 0),
(61, 'U06', 'U06', 1),
(62, 'U06', 'U07', 0),
(63, 'U06', 'U08', 0),
(64, 'U06', 'U09', 0),
(65, 'U06', 'U10', 0),
(66, 'U06', 'U11', 0),
(67, 'U06', 'U12', 0),
(68, 'U06', 'U13', 0),
(69, 'U06', 'U14', 0),
(70, 'U07', 'U07', 1),
(71, 'U07', 'U08', 0),
(72, 'U07', 'U09', 0),
(73, 'U07', 'U10', 0),
(74, 'U07', 'U11', 0),
(75, 'U07', 'U12', 0),
(76, 'U07', 'U13', 0),
(77, 'U07', 'U14', 0),
(78, 'U08', 'U08', 1),
(79, 'U08', 'U09', 0),
(80, 'U08', 'U10', 0),
(81, 'U08', 'U11', 0),
(82, 'U08', 'U12', 0),
(83, 'U08', 'U13', 0),
(84, 'U08', 'U14', 0),
(85, 'U09', 'U09', 1),
(86, 'U09', 'U10', 0),
(87, 'U09', 'U11', 0),
(88, 'U09', 'U12', 0),
(89, 'U09', 'U13', 0),
(90, 'U09', 'U14', 0),
(91, 'U10', 'U10', 1),
(92, 'U10', 'U11', 0),
(93, 'U10', 'U12', 0),
(94, 'U10', 'U13', 0),
(95, 'U10', 'U14', 0),
(96, 'U11', 'U11', 1),
(97, 'U11', 'U12', 0),
(98, 'U11', 'U13', 0),
(99, 'U11', 'U14', 0),
(100, 'U12', 'U12', 1),
(101, 'U12', 'U13', 0),
(102, 'U12', 'U14', 0),
(103, 'U13', 'U13', 1),
(104, 'U13', 'U14', 0),
(105, 'U14', 'U14', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `daemons`
--

CREATE TABLE IF NOT EXISTS `daemons` (
  `Start` text NOT NULL,
  `Info` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_survey`
--

CREATE TABLE IF NOT EXISTS `detail_survey` (
  `id` int(11) NOT NULL,
  `id_survey` int(11) NOT NULL,
  `nomor_soal` int(11) NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `type` varchar(1) NOT NULL,
  `skor` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_survey`
--

INSERT INTO `detail_survey` (`id`, `id_survey`, `nomor_soal`, `jawaban`, `type`, `skor`) VALUES
(1, 27, 1, 'Tidak mudah', 'A', 1),
(2, 5, 2, 'Tidak sesuai', 'A', 1),
(3, 5, 3, 'Sangat Jelas', 'D', 4),
(4, 5, 4, 'Disiplin', 'C', 3),
(5, 8, 5, 'Sangat bertanggung jawab', 'D', 4),
(6, 5, 6, 'Kurang cepat', 'B', 2),
(7, 5, 7, 'Kurang adil', 'B', 2),
(8, 5, 8, 'Sangat sopan dan ramah', 'D', 4),
(9, 5, 9, 'Wajar', 'C', 3),
(10, 5, 10, 'Selalu Tidak sesuai', 'A', 1),
(11, 5, 11, 'Kadang-kadang Tidak tepat', 'B', 2),
(12, 5, 12, 'Nyaman', 'C', 3),
(13, 5, 13, 'Aman', 'C', 3),
(14, 5, 14, 'Mampu', 'C', 3),
(15, 27, 1, 'Tidak mudah', 'A', 1),
(16, 6, 2, 'Tidak sesuai', 'A', 1),
(17, 6, 3, 'Tidak jelas', 'A', 1),
(18, 6, 4, 'Kurang disiplin', 'B', 2),
(19, 8, 5, 'Sangat bertanggung jawab', 'D', 4),
(20, 6, 6, 'Kurang cepat', 'B', 2),
(21, 6, 7, 'Adil', 'C', 3),
(22, 6, 8, 'Sopan dan ramah', 'C', 3),
(23, 6, 9, 'Wajar', 'C', 3),
(24, 6, 10, 'Selalu sesuai', 'D', 4),
(25, 6, 11, 'Selalu tepat', 'D', 4),
(26, 6, 12, 'Sangat nyaman', 'D', 4),
(27, 6, 13, 'Kurang aman', 'B', 2),
(28, 6, 14, 'Mampu', 'C', 3),
(29, 27, 1, 'Tidak mudah', 'A', 1),
(30, 7, 2, 'Tidak sesuai', 'A', 1),
(31, 7, 3, 'Tidak jelas', 'A', 1),
(32, 7, 4, 'Tidak disiplin', 'A', 1),
(33, 8, 5, 'Sangat bertanggung jawab', 'D', 4),
(34, 7, 6, 'Sangat cepat', 'D', 4),
(35, 7, 7, 'Sangat adil', 'D', 4),
(36, 7, 8, 'Sangat sopan dan ramah', 'D', 4),
(37, 7, 9, 'Sangat wajar', 'D', 4),
(38, 7, 10, 'Selalu sesuai', 'D', 4),
(39, 7, 11, 'Selalu tepat', 'D', 4),
(40, 7, 12, 'Sangat nyaman', 'D', 4),
(41, 7, 13, 'Sangat aman', 'D', 4),
(42, 7, 14, 'Sangat Mampu', 'D', 4),
(43, 27, 1, 'Tidak mudah', 'A', 1),
(44, 8, 2, 'Sangat sesuai', 'D', 4),
(45, 8, 3, 'Sangat Jelas', 'D', 4),
(46, 8, 4, 'Sangat disiplin', 'D', 4),
(47, 8, 5, 'Sangat bertanggung jawab', 'D', 4),
(48, 8, 6, 'Sangat cepat', 'D', 4),
(49, 8, 7, 'Sangat adil', 'D', 4),
(50, 8, 8, 'Sangat sopan dan ramah', 'D', 4),
(51, 8, 9, 'Sangat wajar', 'D', 4),
(52, 8, 10, 'Selalu sesuai', 'D', 4),
(53, 8, 11, 'Selalu tepat', 'D', 4),
(54, 8, 12, 'Sangat nyaman', 'D', 4),
(55, 8, 13, 'Sangat aman', 'D', 4),
(56, 8, 14, 'Sangat Mampu', 'D', 4),
(57, 27, 1, 'Tidak mudah', 'A', 1),
(58, 9, 2, 'Kurang sesuai', 'B', 2),
(59, 9, 3, 'Kurang jelas', 'B', 2),
(60, 9, 4, 'Disiplin', 'C', 3),
(61, 9, 5, 'bertanggung jawab', 'C', 3),
(62, 9, 6, 'Tidak cepat', 'A', 1),
(63, 9, 7, 'Tidak adil', 'A', 1),
(64, 9, 8, 'Sopan dan ramah', 'C', 3),
(65, 9, 9, 'Sangat wajar', 'D', 4),
(66, 9, 10, 'Selalu sesuai', 'D', 4),
(67, 9, 11, 'Selalu tepat', 'D', 4),
(68, 9, 12, 'Sangat nyaman', 'D', 4),
(69, 9, 13, 'Sangat aman', 'D', 4),
(70, 9, 14, 'Sangat Mampu', 'D', 4),
(71, 27, 1, 'Tidak mudah', 'A', 1),
(72, 10, 2, 'Sangat sesuai', 'D', 4),
(73, 10, 3, 'Sangat Jelas', 'D', 4),
(74, 10, 4, 'Sangat disiplin', 'D', 4),
(75, 10, 5, 'Sangat bertanggung jawab', 'D', 4),
(76, 10, 6, 'Kurang cepat', 'B', 2),
(77, 10, 7, 'Tidak adil', 'A', 1),
(78, 10, 8, 'Sopan dan ramah', 'C', 3),
(79, 10, 9, 'Wajar', 'C', 3),
(80, 10, 10, 'Selalu sesuai', 'D', 4),
(81, 10, 11, 'Selalu tepat', 'D', 4),
(82, 10, 12, 'Sangat nyaman', 'D', 4),
(83, 10, 13, 'Sangat aman', 'D', 4),
(84, 10, 14, 'Sangat Mampu', 'D', 4),
(85, 27, 1, 'Tidak mudah', 'A', 1),
(86, 11, 2, 'Sangat sesuai', 'D', 4),
(87, 11, 3, 'Sangat Jelas', 'D', 4),
(88, 11, 4, 'Sangat disiplin', 'D', 4),
(89, 11, 5, 'Sangat bertanggung jawab', 'D', 4),
(90, 11, 6, 'Sangat cepat', 'D', 4),
(91, 11, 7, 'Sangat adil', 'D', 4),
(92, 11, 8, 'Sangat sopan dan ramah', 'D', 4),
(93, 11, 9, 'Sangat wajar', 'D', 4),
(94, 11, 10, 'Selalu sesuai', 'D', 4),
(95, 11, 11, 'Selalu tepat', 'D', 4),
(96, 11, 12, 'Sangat nyaman', 'D', 4),
(97, 11, 13, 'Sangat aman', 'D', 4),
(98, 11, 14, 'Sangat Mampu', 'D', 4),
(99, 27, 1, 'Tidak mudah', 'A', 1),
(100, 12, 2, 'Sesuai', 'C', 3),
(101, 12, 3, 'Jelas', 'C', 3),
(102, 12, 4, 'Disiplin', 'C', 3),
(103, 12, 5, 'bertanggung jawab', 'C', 3),
(104, 12, 6, 'Sangat cepat', 'D', 4),
(105, 12, 7, 'Sangat adil', 'D', 4),
(106, 12, 8, 'Sangat sopan dan ramah', 'D', 4),
(107, 12, 9, 'Sangat wajar', 'D', 4),
(108, 12, 10, 'Selalu sesuai', 'D', 4),
(109, 12, 11, 'Selalu tepat', 'D', 4),
(110, 12, 12, 'Tidak nyaman', 'A', 1),
(111, 12, 13, 'Sangat aman', 'D', 4),
(112, 12, 14, 'Sangat Mampu', 'D', 4),
(113, 27, 1, 'Tidak mudah', 'A', 1),
(114, 13, 2, 'Sesuai', 'C', 3),
(115, 13, 3, 'Jelas', 'C', 3),
(116, 13, 4, 'Disiplin', 'C', 3),
(117, 13, 5, 'bertanggung jawab', 'C', 3),
(118, 13, 6, 'Sangat cepat', 'D', 4),
(119, 13, 7, 'Adil', 'C', 3),
(120, 13, 8, 'Sangat sopan dan ramah', 'D', 4),
(121, 13, 9, 'Sangat wajar', 'D', 4),
(122, 13, 10, 'Selalu sesuai', 'D', 4),
(123, 13, 11, 'Selalu tepat', 'D', 4),
(124, 13, 12, 'Sangat nyaman', 'D', 4),
(125, 13, 13, 'Sangat aman', 'D', 4),
(126, 13, 14, 'Sangat Mampu', 'D', 4),
(127, 27, 1, 'Tidak mudah', 'A', 1),
(128, 14, 2, 'Sangat sesuai', 'D', 4),
(129, 14, 3, 'Sangat Jelas', 'D', 4),
(130, 14, 4, 'Tidak disiplin', 'A', 1),
(131, 14, 5, 'Tidak bertanggung jawab', 'A', 1),
(132, 14, 6, 'Tidak cepat', 'A', 1),
(133, 14, 7, 'Tidak adil', 'A', 1),
(134, 14, 8, 'Tidak sopan dan ramah', 'A', 1),
(135, 14, 9, 'Tidak wajar', 'A', 1),
(136, 14, 10, 'Selalu Tidak sesuai', 'A', 1),
(137, 14, 11, 'Banyak  tepatnya', 'C', 3),
(138, 14, 12, 'Nyaman', 'C', 3),
(139, 14, 13, 'Aman', 'C', 3),
(140, 14, 14, 'Mampu', 'C', 3),
(141, 27, 1, 'Tidak mudah', 'A', 1),
(142, 15, 2, 'Sesuai', 'C', 3),
(143, 15, 3, 'Jelas', 'C', 3),
(144, 15, 4, 'Disiplin', 'C', 3),
(145, 15, 5, 'bertanggung jawab', 'C', 3),
(146, 15, 6, 'Sangat cepat', 'D', 4),
(147, 15, 7, 'Sangat adil', 'D', 4),
(148, 15, 8, 'Sangat sopan dan ramah', 'D', 4),
(149, 15, 9, 'Sangat wajar', 'D', 4),
(150, 15, 10, 'Selalu sesuai', 'D', 4),
(151, 15, 11, 'Banyak  tepatnya', 'C', 3),
(152, 15, 12, 'Nyaman', 'C', 3),
(153, 15, 13, 'Aman', 'C', 3),
(154, 15, 14, 'Sangat Mampu', 'D', 4),
(155, 27, 1, 'Tidak mudah', 'A', 1),
(156, 16, 2, 'Sesuai', 'C', 3),
(157, 16, 3, 'Sangat Jelas', 'D', 4),
(158, 16, 4, 'Sangat disiplin', 'D', 4),
(159, 16, 5, 'bertanggung jawab', 'C', 3),
(160, 16, 6, 'Sangat cepat', 'D', 4),
(161, 16, 7, 'Sangat adil', 'D', 4),
(162, 16, 8, 'Sopan dan ramah', 'C', 3),
(163, 16, 9, 'Sangat wajar', 'D', 4),
(164, 16, 10, 'Kadang – kadang sesuai', 'B', 2),
(165, 16, 11, 'Selalu tepat', 'D', 4),
(166, 16, 12, 'Sangat nyaman', 'D', 4),
(167, 16, 13, 'Aman', 'C', 3),
(168, 16, 14, 'Mampu', 'C', 3),
(169, 27, 1, 'Tidak mudah', 'A', 1),
(170, 17, 2, 'Sangat sesuai', 'D', 4),
(171, 17, 3, 'Sangat Jelas', 'D', 4),
(172, 17, 4, 'Sangat disiplin', 'D', 4),
(173, 17, 5, 'Sangat bertanggung jawab', 'D', 4),
(174, 17, 6, 'Sangat cepat', 'D', 4),
(175, 17, 7, 'Sangat adil', 'D', 4),
(176, 17, 8, 'Sopan dan ramah', 'C', 3),
(177, 17, 9, 'Sangat wajar', 'D', 4),
(178, 17, 10, 'Selalu sesuai', 'D', 4),
(179, 17, 12, 'Sangat nyaman', 'D', 4),
(180, 17, 11, 'Selalu tepat', 'D', 4),
(181, 17, 13, 'Sangat aman', 'D', 4),
(182, 17, 14, 'Mampu', 'C', 3),
(183, 27, 1, 'Tidak mudah', 'A', 1),
(184, 18, 2, 'Kurang sesuai', 'B', 2),
(185, 18, 3, 'Jelas', 'C', 3),
(186, 18, 4, 'Kurang disiplin', 'B', 2),
(187, 18, 5, 'Kurang bertanggung jawab', 'B', 2),
(188, 18, 6, 'Cepat', 'C', 3),
(189, 18, 7, 'Adil', 'C', 3),
(190, 18, 8, 'Kurang sopan dan ramah', 'B', 2),
(191, 18, 9, 'Wajar', 'C', 3),
(192, 18, 10, 'Kadang – kadang sesuai', 'B', 2),
(193, 18, 11, 'Selalu Tidak tepat', 'A', 1),
(194, 18, 12, 'Tidak nyaman', 'A', 1),
(195, 18, 13, 'Tidak aman', 'A', 1),
(196, 18, 14, 'Tidak mampu', 'A', 1),
(197, 27, 1, 'Tidak mudah', 'A', 1),
(198, 19, 2, 'Sangat sesuai', 'D', 4),
(199, 19, 3, 'Sangat Jelas', 'D', 4),
(200, 19, 4, 'Sangat disiplin', 'D', 4),
(201, 19, 5, 'Sangat bertanggung jawab', 'D', 4),
(202, 19, 6, 'Sangat cepat', 'D', 4),
(203, 19, 7, 'Kurang adil', 'B', 2),
(204, 19, 8, 'Sopan dan ramah', 'C', 3),
(205, 19, 9, 'Sangat wajar', 'D', 4),
(206, 19, 10, 'Banyak sesuainya', 'C', 3),
(207, 19, 11, 'Selalu tepat', 'D', 4),
(208, 19, 12, 'Sangat nyaman', 'D', 4),
(209, 19, 13, 'Sangat aman', 'D', 4),
(210, 19, 14, 'Sangat Mampu', 'D', 4),
(211, 27, 1, 'Tidak mudah', 'A', 1),
(212, 20, 2, 'Kurang sesuai', 'B', 2),
(213, 20, 3, 'Kurang jelas', 'B', 2),
(214, 20, 4, 'Disiplin', 'C', 3),
(215, 20, 5, 'bertanggung jawab', 'C', 3),
(216, 20, 6, 'Kurang cepat', 'B', 2),
(217, 20, 7, 'Adil', 'C', 3),
(218, 20, 8, 'Kurang sopan dan ramah', 'B', 2),
(219, 20, 9, 'Wajar', 'C', 3),
(220, 20, 10, 'Kadang – kadang sesuai', 'B', 2),
(221, 20, 11, 'Selalu tepat', 'D', 4),
(222, 20, 12, 'Sangat nyaman', 'D', 4),
(223, 20, 13, 'Sangat aman', 'D', 4),
(224, 20, 14, 'Sangat Mampu', 'D', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gammu`
--

CREATE TABLE IF NOT EXISTS `gammu` (
  `Version` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `gammu`
--

INSERT INTO `gammu` (`Version`) VALUES
(13);

-- --------------------------------------------------------

--
-- Struktur dari tabel `inbox`
--

CREATE TABLE IF NOT EXISTS `inbox` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ReceivingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Text` text NOT NULL,
  `SenderNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text NOT NULL,
  `SMSCNumber` varchar(20) NOT NULL DEFAULT '',
  `Class` int(11) NOT NULL DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) unsigned NOT NULL,
  `RecipientID` text NOT NULL,
  `Processed` enum('false','true') NOT NULL DEFAULT 'false',
  `newComing` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `inbox`
--

INSERT INTO `inbox` (`UpdatedInDB`, `ReceivingDateTime`, `Text`, `SenderNumber`, `Coding`, `UDH`, `SMSCNumber`, `Class`, `TextDecoded`, `ID`, `RecipientID`, `Processed`, `newComing`) VALUES
('2016-04-17 13:13:05', '2016-04-17 12:13:11', '007400650073', '+628987069270', 'Default_No_Compression', '', '+628964011092', -1, 'tes', 1, '', 'false', 0),
('2016-04-17 13:14:06', '2016-04-17 12:13:53', '00740065007300740069006E006700200062006F0073', '+628987069270', 'Default_No_Compression', '', '+628964011092', -1, 'testing bos', 2, '', 'false', 0),
('2016-04-17 15:05:17', '2016-04-17 14:03:47', '006B0074006B006D0067006A006D0074006B0061', '+628987069270', 'Default_No_Compression', '', '+628964011092', -1, 'ktkmgjmtka', 3, '', 'false', 0),
('2016-04-17 15:05:17', '2016-04-17 14:04:54', '0074006500730074', '+628987069270', 'Default_No_Compression', '', '+628964011092', -1, 'test', 4, '', 'false', 0),
('2016-04-17 16:52:40', '2016-04-17 15:52:54', '0061006A0061006D', '+628987069270', 'Default_No_Compression', '', '+628964011092', -1, 'ajam', 5, '', 'false', 0),
('2016-04-18 13:16:04', '2016-04-18 12:16:11', '006C006D006A0067', '+628987069270', 'Default_No_Compression', '', '+628964011092', -1, 'lmjg', 7, '', 'false', 0),
('2016-04-18 13:16:50', '2016-04-18 12:17:02', '006900640066006700770065006900670077006F0065006700680077006F006500680067006F00770065006800670065007500720069006700680072007500650069006700720075006900680067007200650069007500680067007200650075006800670065007200680067006500750072006700680075006900650072006800650072006800720065006800650072007500680065007200670068006500670068006500720075006700680065007200670068007200650068007600650072006900760068006500720069006700760065007200670076006500790072006700620065007900720067006200650067006200650072006800620065007200680076006500760068007200650067007600650072007900670072006500790067006500720068007600720065006900760067000A000A0069006E0066006F003A', '+628987069270', 'Default_No_Compression', '', '+628964011092', -1, 'idfgweigwoeghwoehgowehgeurighrueigruihgreiuhgreuhgerhgeurghuierherhreheruherghegherugherghrehverivherigvergveyrgbeyrgbegberhberhvevhregverygreygerhvreivg\n\ninfo:', 8, '', 'false', 0);

--
-- Trigger `inbox`
--
DELIMITER $$
CREATE TRIGGER `inbox_timestamp` BEFORE INSERT ON `inbox`
 FOR EACH ROW BEGIN
    IF NEW.ReceivingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.ReceivingDateTime = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE IF NOT EXISTS `jadwal` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `start_time` time NOT NULL,
  `doe_time` time NOT NULL,
  `durasi` varchar(10) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id`, `tanggal`, `start_time`, `doe_time`, `durasi`, `status`) VALUES
(4, '2016-08-04', '10:00:30', '12:00:30', '2', 1),
(5, '2016-08-05', '21:43:00', '22:40:00', '1', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuesioner`
--

CREATE TABLE IF NOT EXISTS `kuesioner` (
  `id` int(11) NOT NULL,
  `pertanyaan` varchar(225) NOT NULL,
  `A` varchar(225) NOT NULL,
  `B` varchar(225) NOT NULL,
  `C` varchar(225) NOT NULL,
  `D` varchar(225) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kuesioner`
--

INSERT INTO `kuesioner` (`id`, `pertanyaan`, `A`, `B`, `C`, `D`) VALUES
(1, 'Bagaimana pemahaman Saudara tentang kemudahan prosedur pelayanan di unit ini ?', 'Tidak mudah', 'Kurang mudah', 'Mudah', 'Sangat mudah'),
(2, 'Bagaimana pendapat Saudara tentang kesamaan persyaratan pelayanan dengan jenis pelayanannya?', 'Tidak sesuai', 'Kurang sesuai', 'Sesuai', 'Sangat sesuai'),
(3, 'Bagaimana pendapat Saudara tentang kejelasan dan kepastian petugas yang melayani', 'Tidak jelas', 'Kurang jelas', 'Jelas', 'Sangat Jelas'),
(4, 'Bagaimana pendapat Saudara tentang kedisiplinan petugas dalam memberikan pelayanan', 'Tidak disiplin', 'Kurang disiplin', 'Disiplin', 'Sangat disiplin'),
(5, 'Bagaimana pendapat Saudara\ntentang tanggung jawab petugas\ndalam memberikan pelayanan', 'Tidak bertanggung jawab', 'Kurang bertanggung jawab', 'bertanggung jawab', 'Sangat bertanggung jawab'),
(6, 'Bagaimana pendapat Saudara\ntentang kecepatan pelayanan di unit\nini', 'Tidak cepat', 'Kurang cepat', 'Cepat', 'Sangat cepat'),
(7, 'Bagaimana pendapat Saudara tentang\nkeadilan untuk mendapatkan pelayanan\ndisini', 'Tidak adil', 'Kurang adil', 'Adil', 'Sangat adil'),
(8, 'Bagaimana pendapat Saudara tentang\nkesopanan dan keramahan petugas\ndalam memberikan pelayanan', 'Tidak sopan dan ramah', 'Kurang sopan dan ramah', 'Sopan dan ramah', 'Sangat sopan dan ramah'),
(9, 'Bagaimana pendapat Saudara tentang\nkewajaran biaya untuk mendapatkan\npelayanan', 'Tidak wajar', 'Kurang wajar', 'Wajar', 'Sangat wajar'),
(10, 'Bagaimana pendapat Saudara tentang\nkesesuaian antara biaya yang\ndibayarkan dengan biaya yang telah\nditetapkan', 'Selalu Tidak sesuai', 'Kadang – kadang sesuai', 'Banyak sesuainya', 'Selalu sesuai'),
(11, 'Bagaimana pendapat Saudara tentang\nketepatan pelaksanaan terhadap jadwal\nwaktu pelayanan', 'Selalu Tidak tepat', 'Kadang-kadang Tidak tepat', 'Banyak  tepatnya', 'Selalu tepat'),
(12, 'Bagaimana pendapat Saudara tentang\nkenyamanan di lingkungan unit\npelayanan', 'Tidak nyaman', 'Kurang nyaman', 'Nyaman', 'Sangat nyaman'),
(13, 'Bagaimana pendapat Saudara tentang\nkeamanan pelayanan di unit ini', 'Tidak aman', 'Kurang aman', 'Aman', 'Sangat aman'),
(14, 'Bagaimana pendapat Saudara\ntentang kemampuan petugas dalam\nmemberikan pelayanan', 'Tidak mampu', 'Kurang mampu', 'Mampu', 'Sangat Mampu'),
(19, 'iuy', 'ytu', 'yt', 'tuy', 'tut');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_persepsi`
--

CREATE TABLE IF NOT EXISTS `nilai_persepsi` (
  `id` int(11) NOT NULL,
  `type` varchar(1) NOT NULL,
  `nilai` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai_persepsi`
--

INSERT INTO `nilai_persepsi` (`id`, `type`, `nilai`) VALUES
(1, 'A', 4),
(2, 'B', 3),
(3, 'C', 2),
(4, 'D', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `outbox`
--

CREATE TABLE IF NOT EXISTS `outbox` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendBefore` time NOT NULL DEFAULT '23:59:59',
  `SendAfter` time NOT NULL DEFAULT '00:00:00',
  `Text` text,
  `DestinationNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text,
  `Class` int(11) DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) unsigned NOT NULL,
  `MultiPart` enum('false','true') DEFAULT 'false',
  `RelativeValidity` int(11) DEFAULT '-1',
  `SenderID` varchar(255) DEFAULT NULL,
  `SendingTimeOut` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `DeliveryReport` enum('default','yes','no') DEFAULT 'default',
  `CreatorID` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=386 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `outbox`
--

INSERT INTO `outbox` (`UpdatedInDB`, `InsertIntoDB`, `SendingDateTime`, `SendBefore`, `SendAfter`, `Text`, `DestinationNumber`, `Coding`, `UDH`, `Class`, `TextDecoded`, `ID`, `MultiPart`, `RelativeValidity`, `SenderID`, `SendingTimeOut`, `DeliveryReport`, `CreatorID`) VALUES
('2016-08-04 12:24:18', '2016-08-04 12:24:18', '2016-08-04 12:24:18', '23:59:59', '00:00:00', NULL, '098797987987', 'Default_No_Compression', NULL, -1, 'reeyr', 377, 'false', -1, NULL, '2016-08-04 12:24:18', 'default', ''),
('2016-08-04 12:24:38', '2016-08-04 12:24:38', '2016-08-04 12:24:38', '23:59:59', '00:00:00', NULL, '08987069270', 'Default_No_Compression', NULL, -1, 'dfsgws', 378, 'false', -1, NULL, '2016-08-04 12:24:38', 'default', ''),
('2016-08-04 12:32:53', '2016-08-04 12:32:53', '2016-08-04 12:32:53', '23:59:59', '00:00:00', NULL, '087877260241', 'Default_No_Compression', NULL, -1, 'rgre', 379, 'false', -1, NULL, '2016-08-04 12:32:53', 'default', ''),
('2016-08-04 12:32:53', '2016-08-04 12:32:53', '2016-08-04 12:32:53', '23:59:59', '00:00:00', NULL, '089611024206', 'Default_No_Compression', NULL, -1, 'rgre', 380, 'false', -1, NULL, '2016-08-04 12:32:53', 'default', ''),
('2016-08-04 12:32:53', '2016-08-04 12:32:53', '2016-08-04 12:32:53', '23:59:59', '00:00:00', NULL, '085379494675', 'Default_No_Compression', NULL, -1, 'rgre', 381, 'false', -1, NULL, '2016-08-04 12:32:53', 'default', ''),
('2016-08-04 12:32:53', '2016-08-04 12:32:53', '2016-08-04 12:32:53', '23:59:59', '00:00:00', NULL, '087877260243', 'Default_No_Compression', NULL, -1, 'rgre', 382, 'false', -1, NULL, '2016-08-04 12:32:53', 'default', ''),
('2016-08-04 12:32:53', '2016-08-04 12:32:53', '2016-08-04 12:32:53', '23:59:59', '00:00:00', NULL, '098797987987', 'Default_No_Compression', NULL, -1, 'rgre', 383, 'false', -1, NULL, '2016-08-04 12:32:53', 'default', ''),
('2016-08-07 13:32:18', '2016-08-07 13:32:18', '2016-08-07 13:32:18', '23:59:59', '00:00:00', NULL, '0987654', 'Default_No_Compression', NULL, -1, 'uyuyhoi', 385, 'false', -1, NULL, '2016-08-07 13:32:18', 'default', '');

--
-- Trigger `outbox`
--
DELIMITER $$
CREATE TRIGGER `outbox_timestamp` BEFORE INSERT ON `outbox`
 FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.SendingDateTime = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingTimeOut = '0000-00-00 00:00:00' THEN
        SET NEW.SendingTimeOut = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `outbox_multipart`
--

CREATE TABLE IF NOT EXISTS `outbox_multipart` (
  `Text` text,
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text,
  `Class` int(11) DEFAULT '-1',
  `TextDecoded` text,
  `ID` int(10) unsigned NOT NULL DEFAULT '0',
  `SequencePosition` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pairwise`
--

CREATE TABLE IF NOT EXISTS `pairwise` (
  `id` int(11) NOT NULL,
  `skala` varchar(10) NOT NULL,
  `definisi` varchar(225) NOT NULL,
  `keterangan` varchar(225) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pairwise`
--

INSERT INTO `pairwise` (`id`, `skala`, `definisi`, `keterangan`) VALUES
(1, '1', 'Kedua unsur sama penting', 'kedua unsur memiliki pengaruh yang sama'),
(2, '3', 'unsur yang satu sedikit lebih penting daripada yang lainnya', 'Penilaian sangat memihak pada salah satu elemen dibanding pasangannya'),
(3, '5', 'Unsur yang satu lebih penting daripada yang lainnya', 'Penilaian sangat memihak pada salah satu elemenunsur dibanding pasangannya'),
(4, '7', 'Unsur yang satu jelas sangat penting daripada unsur yang lainnya', 'Salah satu unsur sangat berpengaruh dan dominasinya tampak secara nyata'),
(5, '9', 'Unsur yang satu mutlak sangat penting daripada unsur yang lainnya', 'Bukti bahwa salah satu unsur sangat penting daripada pasangannya adalah sangat jelas'),
(6, '2,4,6,8', 'Nilai tengah di antara dua perbandingan yang berdekatan', 'Nilai ini diberikan jika terdapat keraguan di antara kedua peniaian yang berdekatan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pbk`
--

CREATE TABLE IF NOT EXISTS `pbk` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL DEFAULT '-1',
  `Name` text NOT NULL,
  `Number` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pbk`
--

INSERT INTO `pbk` (`ID`, `GroupID`, `Name`, `Number`) VALUES
(13, 6, 'Nur Wasiah', '087877260243'),
(8, 6, 'Aldi', '087877260241'),
(11, 6, 'Kiki', '085379494675'),
(14, 7, 'wildan', '08987069270'),
(15, 7, 'erni', '089611024206'),
(17, 1, 'saya', '098797987987');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pbk_groups`
--

CREATE TABLE IF NOT EXISTS `pbk_groups` (
  `Name` text NOT NULL,
  `ID` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pbk_groups`
--

INSERT INTO `pbk_groups` (`Name`, `ID`) VALUES
('Teman', 7),
('Keluarga', 6),
('Kantor', 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `phones`
--

CREATE TABLE IF NOT EXISTS `phones` (
  `ID` text NOT NULL,
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TimeOut` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Send` enum('yes','no') NOT NULL DEFAULT 'no',
  `Receive` enum('yes','no') NOT NULL DEFAULT 'no',
  `IMEI` varchar(35) NOT NULL,
  `Client` text NOT NULL,
  `Battery` int(11) NOT NULL DEFAULT '-1',
  `Signal` int(11) NOT NULL DEFAULT '1',
  `Sent` int(11) NOT NULL DEFAULT '0',
  `Received` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `phones`
--

INSERT INTO `phones` (`ID`, `UpdatedInDB`, `InsertIntoDB`, `TimeOut`, `Send`, `Receive`, `IMEI`, `Client`, `Battery`, `Signal`, `Sent`, `Received`) VALUES
('Phone1', '2014-03-14 11:05:00', '2014-03-14 06:32:40', '2014-03-14 11:05:10', 'yes', 'yes', '355630020369943', 'Gammu 1.32.0, Windows Server 2007 SP1, GCC 4.7, MinGW 3.11', 100, 63, 33, 8),
('', '2016-04-18 17:40:58', '2016-04-18 15:28:27', '2016-04-18 17:41:08', 'yes', 'yes', '353143036800473', 'Gammu 1.33.0, Windows Server 2007 SP1, GCC 4.7, MinGW 3.11', 0, 30, 0, 1);

--
-- Trigger `phones`
--
DELIMITER $$
CREATE TRIGGER `phones_timestamp` BEFORE INSERT ON `phones`
 FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.TimeOut = '0000-00-00 00:00:00' THEN
        SET NEW.TimeOut = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `responden`
--

CREATE TABLE IF NOT EXISTS `responden` (
  `id` int(11) NOT NULL,
  `nama_responden` varchar(50) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `usia` varchar(10) NOT NULL,
  `alamat` text NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `pendidikan_terakhir` varchar(20) NOT NULL,
  `pekerjaan` varchar(20) NOT NULL,
  `id_subunit` int(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telepon` varchar(12) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `responden`
--

INSERT INTO `responden` (`id`, `nama_responden`, `tempat_lahir`, `tanggal_lahir`, `usia`, `alamat`, `jenis_kelamin`, `pendidikan_terakhir`, `pekerjaan`, `id_subunit`, `email`, `telepon`) VALUES
(20, 'wildan egi ardiawan', 'Bandung', '1992-04-11', '24 Tahun', 'jalan moch toha bandung', 'Laki-Laki', 'D1-D3-D4', 'PNS', 1, 'wildanegi011@gmail.com', '08987069270'),
(2, 'gina zaida yusfira', 'Bandung', '1995-04-21', '20 Tahun', 'jalan raya ciwastra', 'Perempuan', 'S1', 'PNS', 1, 'gina@yahoo.com', '0987897898'),
(3, 'arya beni sanjaya', 'Solo', '1995-05-01', '20 Tahun', 'jalan soreang', 'Laki-Laki', 'S1', 'TNI', 1, 'arya@gmail.com', '09089089889'),
(4, 'pudja ismail', 'Bandung', '1995-09-05', '21 Tahun', 'Jalan Bojong soang', 'Laki-Laki', 'D3', 'POLRI', 1, 'pudja@gmail.com', '0989089089'),
(5, 'Ira Yulianti', 'Bandung', '1995-09-05', '21 Tahun', 'Jalan Cimahi', 'Perempuan', 'S1', 'Wiraswasta', 1, 'ira@gmail.com', '00989878879'),
(6, 'Yuli Pariani', 'Solo', '1996-10-25', '19 Tahun', 'jalan cibaduyut', 'Perempuan', 'S1', 'Wiraswasta', 1, 'yuli@gmail.com', '08767676767'),
(7, 'Swisma Kurniawan', 'Bandung', '1996-10-25', '20 Tahun', 'Jalan Bojong soang', 'Laki-Laki', 'D3', 'TNI', 1, 'swisma@gmail.com', '09897877777'),
(8, 'Wildan April', 'Bandung', '1994-01-02', '22 Tahun', 'Jalan Kiaracondong', 'Laki-Laki', 'D3', 'TNI', 1, 'wildan@gmail.com', '09897989089'),
(9, 'lambok situmorang', 'Bandung', '1994-01-02', '22 Tahun', 'Jalan Soreang', 'Laki-Laki', 'D3', 'PNS', 1, 'lambok@gmail.com', '0989079877'),
(10, 'Gusti Ayu eka', 'Bandung', '1995-04-21', '20 Tahun', 'Jalan babakan Siliwangi', 'Perempuan', 'D3', 'Wiraswasta', 1, 'ayu@yahoo.com', '0987789898'),
(11, 'Rohmat Suteja Somantri', 'Bandung', '1995-04-21', '20 Tahun', 'Jalan Leuwi Panjang', 'Laki-Laki', 'S1', 'PNS', 1, 'rohmat@gmail.com', '09808988898'),
(12, 'Kelvin Alfian', 'Purwakarta', '1996-04-21', '20 Tahun', 'Jalan Sukaati', 'Laki-Laki', 'S1', 'PNS', 1, 'kelvin@gmail.com', '0989899897'),
(13, 'Try Setyo Utomo', 'Bandung', '1996-04-21', '20 Tahun', 'Jalan Cibereum', 'Laki-Laki', 'S1', 'PNS', 1, 'try@gmail.com', '09879789787'),
(14, 'Asri Nur Azmi', 'Bandung', '1996-04-21', '20 Tahun', 'Jalan cimahi', 'Perempuan', 'D3', 'Wiraswasta', 1, 'aci@gmail.com', '09898908989'),
(15, 'Ardi Kurniawan', 'Bandung', '1996-04-21', '20 Tahn', 'jalan Gede Bage', 'Laki-Laki', 'D3', 'POLRI', 1, 'ardi@gmail.com', '0987789777'),
(21, 'alopurinol', 'Bandung', '1992-04-11', '0', '\r\nuiy', 'Laki-Laki', 'SLTA', 'PNS', 1, 'al@gmail.com', '08987069270');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sentitems`
--

CREATE TABLE IF NOT EXISTS `sentitems` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DeliveryDateTime` timestamp NULL DEFAULT NULL,
  `Text` text NOT NULL,
  `DestinationNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text NOT NULL,
  `SMSCNumber` varchar(20) NOT NULL DEFAULT '',
  `Class` int(11) NOT NULL DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) unsigned NOT NULL DEFAULT '0',
  `SenderID` varchar(255) NOT NULL,
  `SequencePosition` int(11) NOT NULL DEFAULT '1',
  `Status` enum('SendingOK','SendingOKNoReport','SendingError','DeliveryOK','DeliveryFailed','DeliveryPending','DeliveryUnknown','Error') NOT NULL DEFAULT 'SendingOK',
  `StatusError` int(11) NOT NULL DEFAULT '-1',
  `TPMR` int(11) NOT NULL DEFAULT '-1',
  `RelativeValidity` int(11) NOT NULL DEFAULT '-1',
  `CreatorID` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `sentitems`
--

INSERT INTO `sentitems` (`UpdatedInDB`, `InsertIntoDB`, `SendingDateTime`, `DeliveryDateTime`, `Text`, `DestinationNumber`, `Coding`, `UDH`, `SMSCNumber`, `Class`, `TextDecoded`, `ID`, `SenderID`, `SequencePosition`, `Status`, `StatusError`, `TPMR`, `RelativeValidity`, `CreatorID`) VALUES
('2016-04-17 12:09:17', '2016-04-17 12:08:51', '2016-04-17 12:09:17', NULL, '006900640066006700770065006900670077006F0065006700680077006F006500680067006F00770065006800670065007500720069006700680072007500650069006700720075006900680067007200650069007500680067007200650075006800670065007200680067006500750072006700680075006900650072006800650072006800720065006800650072007500680065007200670068006500670068006500720075006700680065007200670068007200650068007600650072006900760068006500720069006700760065007200670076006500790072006700620065007900720067006200650067006200650072006800620065007200680076006500760068007200650067007600650072007900670072006500790067006500720068007600720065006900760067000A000A0069006E0066006F003A0020004200610070006500720069006E', '08987069270', 'Default_No_Compression', '', '+6289644000001', -1, 'idfgweigwoeghwoehgowehgeurighrueigruihgreiuhgreuhgerhgeurghuierherhreheruherghegherugherghrehverivherigvergveyrgbeyrgbegberhberhvevhregverygreygerhvreivg\n\ninfo: Baperin', 375, '', 1, 'SendingOKNoReport', -1, 87, 255, ''),
('2016-04-17 12:12:49', '2016-04-17 12:11:51', '2016-04-17 12:12:49', NULL, '00680062006800670062007500790067006200660068006200660075006900670062006800670066006E0068006700660062006E00660067006400660068006700620064006600680066006B00670068006500720069007900680067006500720068006200720074006800620072006800620066006A00620064006600680062006B007500730064006800620075006400730066006800620064006200680064006B006A00660062006800640066006B00620068006400660068006200640066006B00620064006B00660062006800680064006B0066006200680064006B00660062006E006A006B00640062006E006B006C00640062006A006400660062006C00640066006200640062006A0064006C006A0066006200640066006A0062006B006C00640062006A006D006C006B00640066000A000A0069006E0066006F003A0020004200610070006500720069006E', '08987069270', 'Default_No_Compression', '', '+6289644000001', -1, 'hbhgbuygbfhbfuigbhgfnhgfbnfgdfhgbdfhfkgheriyhgerhbrthbrhbfjbdfhbkusdhbudsfhbdbhdkjfbhdfkbhdfhbdfkbdkfbhhdkfbhdkfbnjkdbnkldbjdfbldfbdbjdljfbdfjbkldbjmlkdf\n\ninfo: Baperin', 376, '', 1, 'SendingError', -1, -1, 255, '');

--
-- Trigger `sentitems`
--
DELIMITER $$
CREATE TRIGGER `sentitems_timestamp` BEFORE INSERT ON `sentitems`
 FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.SendingDateTime = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_unit`
--

CREATE TABLE IF NOT EXISTS `sub_unit` (
  `id` int(11) NOT NULL,
  `sub_unit` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `koordinator` varchar(50) NOT NULL,
  `mesin_peralatan` varchar(10) NOT NULL,
  `potensi_ikm` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_unit`
--

INSERT INTO `sub_unit` (`id`, `sub_unit`, `alamat`, `telepon`, `koordinator`, `mesin_peralatan`, `potensi_ikm`, `email`) VALUES
(1, 'Logam Bandung', 'Jl.Soekarno Hatta km 12,5 Kota Bandung', '0227812787', 'Nining', '7 unit', '210 unit usaha', 'nining@gmail.com'),
(2, 'Logam Bogor', 'Jl. Industri No.55 Ds.Tarikolot, Kec.Citereup Kab. Bogor', '0218794070', 'Lesmana', '7 unit', '250 unit usaha', 'lesmana@gmail.com'),
(3, 'Logam Sukabumi', 'Jln. Siliwangi No. 133 Cisaat Kab. Sukabumi', '0266 222396', 'Joko Witarso', '32 unit', '639 unit usaha', 'joko@gmail.com'),
(4, 'Persepatuan Cibaduyut', 'Jln. Raya Cibaduyut No. 150 Kota Bandung', '022 5406096', 'alex ibrahim', '71 unit', '850 unit usaha', 'alex@gmail.com'),
(5, 'Rotan Cirebon', 'Jln. Tegal wamgi No.1 Kab. Cirebon', '0231 324230', 'agus', '13 unit', '352 unit usaha', 'agus@gmail.com'),
(6, 'Perkayuan Sumedang', 'J\0l\0n\0.\0 \0R\0a\0y\0a\0 \0L\0e\0g\0o\0k\0 \0  \0C\0o\0n\0g\0g\0e\0a\0n\0g\0 \0K\0m\0.\0 \01\0 \0K\0a\0b\0.\0 \0S\0u\0m\0e\0d\0a\0n\0g\0', '0261 201829', 'ruhiyat', '18 unit', '90 unit usaha', 'ruhiyat@gmail.com'),
(7, 'Texstil Majalaya', 'Jln. Babakan No. 41 Majalaya Kab. Bandung', '022 5953231', 'ade yaya', '16 unit', '210 unit usaha', 'ade@gmail.com'),
(8, 'Penyamakan Kullit Garit', 'Jln. Gagak Lumayung Km. 1,5 Sukaregang Kab.Garut', '02262 2245167', 'erik wahyu', '13 unit', '178 unit usaha', 'erik@gmail.com'),
(9, 'Kerajinan Tasikmalaya', 'Jln. Perintis Kemerdekaan Km. 5 Kota Tasikmalaya', '0265 331649', 'eka', '6 unit', '960 unit usaha', 'eka@gmail.com'),
(10, 'Rumah Kemasan', 'Jln. Parabon III No.1 Kota Bandung', '02234758656', 'Detty Tatiananda, SE', '18 unit', '256 unit usaha', 'detty@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `survey`
--

CREATE TABLE IF NOT EXISTS `survey` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(5) NOT NULL,
  `nama_responden` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `total_nilai` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `survey`
--

INSERT INTO `survey` (`id`, `id_jadwal`, `nama_responden`, `email`, `total_nilai`) VALUES
(1, 1, 'samsul la', 'samsul@gmail.com', 0),
(2, 1, 'samsul la', 'samsul@gmail.com', 0),
(3, 1, 'samsul la', 'samsul@gmail.com', 0),
(4, 1, 'samsul la', 'samsul@gmail.com', 0),
(5, 1, 'samsul la', 'samsul@gmail.com', 0),
(6, 1, 'vanessa axe', 'vanes@yahoo.com', 0),
(7, 5, 'Asri Nur Azmi', 'aci@gmail.com', 0),
(8, 5, 'Ardi Kurniawan', 'ardi@gmail.com', 0),
(9, 5, 'arya beni sanjaya', 'arya@gmail.com', 0),
(10, 5, 'Gusti Ayu eka', 'ayu@yahoo.com', 0),
(11, 5, 'gina zaida yusfira', 'gina@yahoo.com', 0),
(12, 5, 'Ira Yulianti', 'ira@gmail.com', 0),
(13, 5, 'Kelvin Alfian', 'kelvin@gmail.com', 0),
(14, 5, 'lambok situmorang', 'lambok@gmail.com', 0),
(15, 5, 'pudja ismail', 'pudja@gmail.com', 0),
(16, 5, 'Rohmat Suteja Somantri', 'rohmat@gmail.com', 0),
(17, 5, 'Swisma Kurniawan', 'swisma@gmail.com', 0),
(18, 5, 'Try Setyo Utomo', 'try@gmail.com', 0),
(19, 5, 'wildan egi ardiawan', 'wildanegi011@gmail.com', 0),
(20, 5, 'Yuli Pariani', 'yuli@gmail.com', 0),
(21, 5, 'Gusti Ayu eka', 'ayu@yahoo.com', 0),
(22, 5, 'Gusti Ayu eka', 'ayu@yahoo.com', 0),
(23, 5, 'Gusti Ayu eka', 'ayu@yahoo.com', 0),
(24, 5, 'Gusti Ayu eka', 'ayu@yahoo.com', 0),
(25, 5, 'Gusti Ayu eka', 'ayu@yahoo.com', 0),
(26, 5, 'Gusti Ayu eka', 'ayu@yahoo.com', 0),
(27, 5, 'Gusti Ayu eka', 'ayu@yahoo.com', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `unsur`
--

CREATE TABLE IF NOT EXISTS `unsur` (
  `id_pertanyaan` int(11) NOT NULL,
  `id_unsur` varchar(10) NOT NULL,
  `nama_unsur` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `unsur`
--

INSERT INTO `unsur` (`id_pertanyaan`, `id_unsur`, `nama_unsur`) VALUES
(1, 'U01', 'Prosedur Pelayanan'),
(2, 'U02', 'Kesesuaian Persyaratan Dengan Pelayanan'),
(3, 'U03', 'Kejelasan Dan Kepastian Petugas Pelayanan'),
(4, 'U04', 'Kedisiplinan Petugas Pelayanan'),
(5, 'U05', 'Tanggung Jawab Petugas Pelayanan'),
(6, 'U06', 'Kemampuan Petugas Pelayanan'),
(7, 'U07', 'Kecepatan Pelayanan'),
(8, 'U08', 'Keadilan Mendapatkan Pelayanan'),
(9, 'U09', 'Kesopanan Dan Keramahan'),
(10, 'U10', 'Kewajaran Biaya Pelayanan'),
(11, 'U11', 'Kesesuaian Biaya Pelayanan'),
(12, 'U12', 'Ketepatan Jadwal Pelayanan'),
(13, 'U13', 'Kenyamanan Lingkungan'),
(14, 'U14', 'Keamanan Pelayanan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`email`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `email_2` (`email`), ADD KEY `username` (`email`);

--
-- Indexes for table `ahp`
--
ALTER TABLE `ahp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_survey`
--
ALTER TABLE `detail_survey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kuesioner`
--
ALTER TABLE `kuesioner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai_persepsi`
--
ALTER TABLE `nilai_persepsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outbox`
--
ALTER TABLE `outbox`
  ADD PRIMARY KEY (`ID`), ADD KEY `outbox_date` (`SendingDateTime`,`SendingTimeOut`), ADD KEY `outbox_sender` (`SenderID`);

--
-- Indexes for table `outbox_multipart`
--
ALTER TABLE `outbox_multipart`
  ADD PRIMARY KEY (`ID`,`SequencePosition`);

--
-- Indexes for table `pairwise`
--
ALTER TABLE `pairwise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pbk`
--
ALTER TABLE `pbk`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pbk_groups`
--
ALTER TABLE `pbk_groups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`IMEI`);

--
-- Indexes for table `responden`
--
ALTER TABLE `responden`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `email_2` (`email`);

--
-- Indexes for table `sentitems`
--
ALTER TABLE `sentitems`
  ADD PRIMARY KEY (`ID`,`SequencePosition`), ADD KEY `sentitems_date` (`DeliveryDateTime`), ADD KEY `sentitems_tpmr` (`TPMR`), ADD KEY `sentitems_dest` (`DestinationNumber`), ADD KEY `sentitems_sender` (`SenderID`);

--
-- Indexes for table `sub_unit`
--
ALTER TABLE `sub_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unsur`
--
ALTER TABLE `unsur`
  ADD PRIMARY KEY (`id_pertanyaan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ahp`
--
ALTER TABLE `ahp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `detail_survey`
--
ALTER TABLE `detail_survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=226;
--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kuesioner`
--
ALTER TABLE `kuesioner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `nilai_persepsi`
--
ALTER TABLE `nilai_persepsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `outbox`
--
ALTER TABLE `outbox`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=386;
--
-- AUTO_INCREMENT for table `pairwise`
--
ALTER TABLE `pairwise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pbk`
--
ALTER TABLE `pbk`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pbk_groups`
--
ALTER TABLE `pbk_groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `responden`
--
ALTER TABLE `responden`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `sub_unit`
--
ALTER TABLE `sub_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `survey`
--
ALTER TABLE `survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `unsur`
--
ALTER TABLE `unsur`
  MODIFY `id_pertanyaan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
